<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/create-symlink', function () {
    Artisan::call('storage:link');
});

Route::get('/migratefresh', function () {
    Artisan::call('migrate:fresh');
});

Route::get('/seeder', function(){
    Artisan::call('db:seed');
});

Route::get('/down', function(){
    Artisan::call('down');
});

// Route::get('/up', function(){
//     Artisan::call('up');
// });


Auth::routes();

Route::prefix('admin')->middleware('auth')->group(function(){
    //You must be logged in to access this route
    Route::get('/index', 'AdminController@index')->name('admin.index');

    Route::resource('pages', 'PageController');

    Route::resource('categories', 'CategoryController');

    Route::resource('slides', 'SlideController');

    Route::resource('posts', 'PostController');
    
    Route::resource('menus', 'MenuController');

    Route::resource('comments', 'CommentController', ['except'=>['store']]);

    Route::resource('users', 'UserController');

    Route::get('users/profile/{user}', 'UserController@profile')->name('users.profile');

    Route::patch('users/profile/{user}', 'UserController@updateProfile')->name('users.updateProfile');

    Route::get('users/password/{user}', 'UserController@changePassword')->name('users.changePassword');

    Route::patch('users/password/{user}', 'UserController@postChangePassword')->name('users.postChangePassword');

    Route::patch('comments/approve/{comment}', 'CommentController@approve')->name('comments.approve');

    Route::patch('comments/disapprove/{comment}', 'CommentController@disapprove')->name('comments.disapprove');
    
    Route::resource('postviews', 'PostViewController');

    Route::delete('postviews-deleteall', 'PostViewController@deleteall')->name('postviews.deleteall');
    
    Route::resource('settings', 'SettingController');

    Route::resource('contactus', 'ContactUsController');

    Route::get('contactus/{contactus}/reply', 'ContactUsController@reply')->name('contactus.reply');

    Route::post('message/send', 'ChatController@send');
    
});

Route::post('comments/store', 'CommentController@store')->name('comments.store');
Route::post('comments/reply', 'CommentController@reply')->name('comments.reply');

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'FrontendController@index')->name('frontend.index');
// Route::get('/page/{menu}/', 'FrontendController@urlmapper')->name('frontend.urlmapper');
Route::get('{menuslug}', 'FrontendController@urlmapper')->name('frontend.urlmapper');

Route::get('posts/{slug}', 'FrontendController@showpost')->name('frontend.posts.show');

Route::get('posts/category/{slug}', 'FrontendController@showcategoryposts')->name('frontend.category.posts');