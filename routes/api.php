<?php

use Illuminate\Http\Request;
use App\Page;
use App\Post;
use App\Category;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::middleware('auth:api')->get('/getdatafromdb', function (Request $request) {
//     return 'Hello World';
//     if ($request->type=='page') {
//         $data = Page::orderBy('title')->get();
//     } elseif($request->type=='post') {
//         $data = Post::orderBy('title')->get();
//     }elseif($request->type=='post category'){
//         $data = Category::orderBy('name')->get();
//     }elseif($request->type=='post category'){
//         $data = Category::orderBy('name')->get();
//     }
    
//     return response()->json($data);
// });

Route::get('/getdatafromdb', function(Request $request){
    if ($request->type=='page') {
        $data = Page::orderBy('title')->get(['id', 'title']);
    } elseif($request->type=='post') {
        $data = Post::orderBy('title')->get(['id', 'title']);
    }elseif($request->type=='post category'){
        $data = Category::orderBy('name')->get(['id', 'name as title']);
    }
    
    return response()->json($data);
});

//Api to create a category
Route::post('category/create', function(Request $request){
    //return $request->newcategory;
    Category::create([
        'name'=>$request->newcategory,
        'parent_id'=>$request->parent_category,
    ]);

    return response()->json('success'); 
});

//Api to get all categories that the user has created
Route::get('categories', function(){
    $categories=Category::orderBy('name')->get();
    return response()->json($categories);   
});

Route::get('postcategories', function(Request $request){
    $post=Post::find($request->id);
    return response()->json($post->categories);
});
