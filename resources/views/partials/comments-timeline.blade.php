<!-- timeline item -->
<li>
    <a href="#"><img src="{{asset('storage/'.$comment->user->photo) }}" alt="" class="img-responsive img-circle"></a>
    {{-- <i class="fa fa-envelope bg-blue"></i> --}}
    <div class="timeline-item">
        <span class="time"><i class="fa fa-clock-o"></i> {{ $comment->created_at->format('h:ia') }}</span>

        <h3 class="timeline-header"><a href="#">{{ $comment->user->username }}</a></h3>

        <div class="timeline-body">
            {{ $comment->content }}
        </div>
        <div class="timeline-footer">
            <div class="row">
                <div class="col-xs-1">
                    <form action="{{ route('comments.approve', ['id'=>$comment->id]) }}" method="post">
                        @csrf
                        @method('PATCH')
                        <button type="submit" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Approve</button>
                    </form>
                </div>
                <div class="col-xs-2">
                    <form action="{{ route('comments.disapprove', ['id'=>$comment->id]) }}" method="post">
                        @csrf
                        @method('PATCH')
                        <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> Disapprove</button>
                    </form>
                </div>
                <div class="col-xs-1">
                    <form method="post" action="{{ route('comments.destroy', $comment->id) }}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger btn-xs" onClick='return confirm("Are you sure you want to delete this comment?")'><i class="fa fa-trash"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</li>
<!-- END timeline item -->
@if($comment->parent()->count())
    @if(!$comment->created_at->isSameDay($comment->parent->created_at))
        @php
            $colours = array('red', 'green', 'yellow', 'blue');
            $random_array_index = array_rand($colours);
        @endphp
        <!-- timeline time label -->
        <li class="time-label">
            <span class="bg-{{$colours[$random_array_index] }}">
                {{ $comment->parent->created_at->format('d M. Y') }}
            </span>
        </li>
        <!-- /.timeline-label -->
    @endif
    @include('partials.comments-timeline',['comment'=>$comment->parent])
@endif
