@if (count($errors) > 0)
    <style>
        .text{
            color: white; 
            display: inline-block; 
            margin-right: 10px;
        }
    </style>

    <div class="errorbag" style="padding: 20px 30px; background: rgb(243, 156, 18) none repeat scroll 0% 0%; z-index: 999999; font-size: 16px; font-weight: 600;">
        <a class="pull-right cancel" href="#" data-toggle="tooltip" data-placement="left" style="color: rgb(255, 255, 255); font-size: 20px;">×</a>
        @foreach ($errors->all() as $error)
            <p style="color:#fff;">{{ $error }}</p>
        @endforeach
    </div>

    @section('succesAndErrorbagScripts')
        <script>
            $('.cancel').click(function(){
                $('.errorbag').fadeOut('slow');
            });
        </script>
    @endsection
@endif
    

