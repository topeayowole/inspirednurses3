@if (session('success'))
    <style>
        .text{
            color: white; 
            display: inline-block; 
            margin-right: 10px;
        }
    </style>

    <div class="successmsg" style="padding: 20px 30px; background: rgb(0, 166, 90) none repeat scroll 0% 0%; z-index: 999999; font-size: 16px; font-weight: 600;">
        <a class="pull-right cancel" href="#" data-toggle="tooltip" data-placement="left" style="color: rgb(255, 255, 255); font-size: 20px;">×</a>
        <p style="color:#fff;">{{ session('success')}}</p>
    </div>

    @section('succesAndErrorbagScripts')
        <script>
            $('.successmsg').delay(5000).fadeOut('slow');

            $('.cancel').click(function(){
                $('.successmsg').fadeOut('slow');
            });
        </script>
    @endsection
@endif
    

