<!-- Carousel -->
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
        @for($i = 0; $i<$slides->count(); $i++)
            <li data-target="#carousel-example-generic" data-slide-to="{{$i}}" {!!$i==0?'class="active"':''!!}></li>
        @endfor
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            @foreach($slides as $index=>$slide)
            <div class="item {{$index==0?'active':''}}">
            <img src="{{asset('storage/'.$slide->image)}}" alt="">
            <div class="carousel-caption">
                <p>{{$slide->caption}}</p>
            </div>
            </div>
        @endforeach
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"><!--<i class="fa fa-chevron-left"></i>--></span>
        <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"><!-- <i class="fa fa-chevron-right"></i> --></span>
        <span class="sr-only">Next</span>
        </a>
    </div>
<!-- end carousel -->
