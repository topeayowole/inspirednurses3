@extends('layouts.dashboard')

@section('content')
    @include('partials.successmsg')
    @include('partials.errorbag')
    <section class="content-header">
        <h1>Edit Slide</h1> 
    </section>

    {{-- Main content --}}
    <section class="content">
        <div class="row">
            <div class="col-md-5">
                <div class="box">
                    <div class="box-body">
                        <form action="{{ route('slides.update', $slide->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')
                            <div class="form-group">
                                <label>Caption</label>
                                <input type="text" name="caption" id="" class="form-control" value="{{$slide->caption}}">
                            </div>

                            <div class="form-group">
                                <label>Subcaption</label>
                                <input type="text" name="subcaption" id="" class="form-control" value="{{$slide->subcaption}}">
                            </div>

                            <div class="form-group">
                                <label>Add image</label>
                                <img src="{{ asset('storage/'.$slide->image) }}" width="100" class="img-rounded">
                                <input type="file" name="image" id="">
                            </div>

                            <div class="form-group">
                                <label>Visibility (visible on home page)</label>
                                <input type="radio" name="visibility" value="1" {{ $slide->visibility==1 ? 'checked' : '' }}> Visible &nbsp;
                                <input type="radio" name="visibility" value="0" {{ $slide->visibility==0 ? 'checked' : '' }}> Hidden
                            </div>

                            <div class="form-group">
                                <label>Order</label>
                                <input type="number" name="order" class="form-control" min="1" value="{{$slide->order}}">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-info">Update Slide</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection