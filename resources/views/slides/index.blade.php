@extends('layouts.dashboard')

@section('content')
    @include('partials.successmsg')

    <section class="content-header">
        <h1>Slides <a href="{{route('slides.create')}}" class="btn btn-info">Add New</a></h1> 
    </section>

    {{-- Main content --}}
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-responsive">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="" id=""></th>
                                <th>Image</th>
                                <th>Caption</th>
                                <th>Visibility</th>
                                <th>Order</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($slides as $slide)
                                <tr>
                                    <td><input type="checkbox" name="" id=""></td>
                                    <td>
                                        <img src="{{ asset('storage/'.$slide->image) }}" width="100" class="img-rounded">
                                    </td>
                                    <td>{{ $slide->caption }}</td>
                                    <td>{{ $slide->visibility }}</td>
                                    <td>{{ $slide->order }}</td>
                                    <td>
                                        <a href="{{ route('slides.edit', $slide->id) }}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                    </td>
                                    <td>
                                        <form id="deleteForm" method="post" action="{{ route('slides.destroy', $slide->id) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger" onClick='return confirm("Are you sure you want to delete?")'><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection