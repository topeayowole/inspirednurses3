@extends('layouts.dashboard')

@section('content')
    @include('partials.successmsg')
    @include('partials.errorbag')
    <section class="content-header">
        <h1>Add New Slide</h1> 
    </section>

    {{-- Main content --}}
    <section class="content">
        <div class="row">
            <div class="col-md-5">
                <div class="box">
                    <div class="box-header">
                        <div class="box-title">Add new slide</div>
                    </div>
                    <div class="box-body">
                        <form action="{{ route('slides.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label>Caption</label>
                                <input type="text" name="caption" id="" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Subcaption</label>
                                <input type="text" name="subcaption" id="" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Add image</label>
                                <input type="file" name="image" id="">
                            </div>

                            <div class="form-group">
                                <label>Visibility (visible on home page)</label>
                                <input type="radio" name="visibility" value="1" checked> &nbsp; Visible &nbsp;
                                <input type="radio" name="visibility" value="0"> &nbsp; Hidden
                            </div>

                            <div class="form-group">
                                <label>Order</label>
                                <input type="number" name="order" class="form-control" min="1" value="1">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-info">Add Slide</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection