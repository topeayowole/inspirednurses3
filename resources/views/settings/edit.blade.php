@extends('layouts.dashboard')

@section('content')
    @include('partials.successmsg')
    @include('partials.errorbag')
    <section class="content-header">
        <h1>Settings</h1> 
    </section>

    {{-- Main content --}}
    <section class="content">
        <form action="{{ route('settings.update', $setting->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="">Site Name</label>
                        <input type="text" name="sitename" class="form-control" value="{{$setting->sitename}}" required>
                    </div>
                    <div class="form-group">
                        <label for="">Theme</label>
                        <select name="themename" class="form-control">
                            @foreach ($foldernames as $name)
                                <option value="{{$name}}">{{$name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Logo</label>
                        <input type="file" name="logo" id="">
                    </div>
                    <div class="form-group">
                        <label for="">Description</label> <br>
                        <textarea name="description" id="description" cols="90" rows="10">{{$setting->description}}</textarea>
                    </div>
                    <p style="font-style: italic">For Developers...</p>
                    <div class="form-group">
                        <label for="">Style Sheet</label> <br>
                        <textarea name="styles" id="styles" cols="90" rows="30">{{$setting->styles}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="">JavaScript</label> <br>
                        <textarea name="javascript" id="javascript" cols="90" rows="30">{{$setting->javascript}}</textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection