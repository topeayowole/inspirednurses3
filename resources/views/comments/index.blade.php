@extends('layouts.dashboard')

@section('styles')
    @include('partials.datatableStyles')
@endsection

@section('content')
    @include('partials.successmsg')
    <section class="content-header">
        <h1>Comments</h1> 
    </section>

    {{-- Main content --}}
    <section class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-responsive">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="" id=""></th>
                                <th>Author</th>
                                <th>Email</th>
                                <th>Comment</th>
                                <th>Post</th>
                                <th>In Response To</th>
                                <th>Submitted On</th>
                                <th>Status</th>
                                <th>Actions</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($comments as $comment)
                                <tr>
                                    <td><input type="checkbox" name="" id=""></td>
                                    <td>{{ $comment->user->username }}</td>
                                    <td>{{ $comment->user->email }}</td>
                                    <td title="{{$comment->content}}" style="cursor:pointer">{{ Str::words($comment->content, 3) }}</td>
                                    <td>
                                        <p>{{ $comment->post->title}}</p>
                                        <a href="{{ route('posts.show', ['id'=>$comment->post->id]) }}">View Post</a>
                                    </td>
                                    <td title="{{$comment->parent!=null ? $comment->parent->content : ''}}" style="cursor:pointer">{{ $comment->parent!=null ? Str::words($comment->parent->content, 3) : ''}}</td>
                                    <td>{{ $comment->created_at->format('Y/m/d \a\t h:iA') }} </td>
                                    <td><span class='label label-{{$comment->status=='Approved' ? "success" : "danger"}}'>{{ $comment->status }}</span></td>
                                    <td><a href="{{ route('comments.show', ['id'=>$comment->id]) }}" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View</a></td>
                                    <td>
                                        <form action="{{ route('comments.approve', ['id'=>$comment->id]) }}" method="post">
                                            @csrf
                                            @method('PATCH')
                                            <button type="submit" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Approve</button>
                                        </form>
                                    </td>
                                    <td>
                                        <form action="{{ route('comments.disapprove', ['id'=>$comment->id]) }}" method="post">
                                            @csrf
                                            @method('PATCH')
                                            <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> Disapprove</button>
                                        </form>
                                    </td>
                                    <td>
                                        <form method="post" action="{{ route('comments.destroy', $comment->id) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-xs" onClick='return confirm("Are you sure you want to delete this comment?")'><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="12">No post comment.</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    @include('partials.datatableScripts')
@endsection