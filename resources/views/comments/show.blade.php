@extends('layouts.dashboard')

@section('content')
    @include('partials.errorbag')
    @include('partials.successmsg')
    <section class="content-header">
        <h1>Comments</h1>
    </section>

    {{-- Main content --}}
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- The time line -->
                <ul class="timeline">
                    <!-- timeline time label -->
                    <li class="time-label">
                        <span class="bg-red">
                            {{ $comment->created_at->format('d M. Y g:i A') }}
                        </span>
                    </li>
                    <!-- /.timeline-label -->
                    @include('partials.comments-timeline',['comment'=>$comment])
                    <li>
                        <i class="fa fa-clock-o bg-gray"></i>
                    </li>
                </ul>
            </div>
        </div>
        <form action="{{ route('comments.update', $comment->id) }}" method="post">
            @csrf
            @method('PATCH')
            <div class="row">
            </div>
        </form>
    </section>
@endsection