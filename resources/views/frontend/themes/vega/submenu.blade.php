<ul class="dropdown-menu" role="menu">
    @foreach ($children as $child)
        <li class="menu-item menu-item-type-custom nav-item {{ $child->children()->count() ? 'menu-item-has-children dropdown' : 'menu-item-object-custom' }}">
            <a href="{{url($menu->slug)}}">{{ $child->name }}</a>
            @if($child->children()->count())
                @include('frontend/themes/'.$frontendpage->setting->themename.'/submenu', ['children'=>$child->children()->orderBy('order')->get()])
            @endif
        </li>
    @endforeach
</ul>