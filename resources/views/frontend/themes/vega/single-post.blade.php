<!-- ========== Banner - Custom Header ========== -->
<div class="jumbotron image-banner banner-custom-header" style="background:url('{{asset('themes/vega/images/cropped-country-road-1149667_1920-1.jpg')}}') no-repeat 0 0 #ffffff;background-size:cover;background-position:center center">
    <div class="container">
        <h1 class="block-title wow zoomIn">{{$frontendpage->post->title}}</h1>
    </div>
</div>
<!-- ========== /Banner - Custom Header ========== -->

<!-- ========== Content Starts ========== -->
<div class="section post-content bg-white">
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div id="post-290" class="clearfix post-290 post type-post status-publish format-standard has-post-thumbnail hentry category-sample-posts category-uncategorized">
                    <!-- Post Title -->
                    <h3 class="entry-title">{{$frontendpage->post->title}}</h3>
                    <!-- /Post Title -->

                    <!-- Post Meta -->
                    <div class="entry-meta">
                        Posted By: <span class="vcard author author_name"><span class="fn">{{$frontendpage->post->user->username}}</span></span>
                        <span class="sep">/</span>
                        Posted Date: <span class="date updated">{{$frontendpage->post->created_at->format('M d, Y')}}</span>
                        <span class="sep">/</span>
                        Under:
                        @forelse ($frontendpage->post->categories as $category)
                            <a href="{{route('frontend.category.posts', $category->slug)}}">{{$category->name}}</a>, 
                        @empty
                            Uncategorised
                        @endforelse
                        <span class="sep">/</span>
                        <i class="ion ion-ios-eye"></i> {{ $frontendpage->post->postViews()->count() }}
                    </div>
                    <!-- /Post Meta -->

                    <!-- Post Tags -->
                    <div class="entry-tags">
                        @foreach ($frontendpage->post->tags as $tag)

                        @endforeach
                    </div>
                    <!-- /Post Tags -->


                    <!-- Post Image -->
                        <div class="entry-image">
                            <img width="800" height="533" src="{{asset('storage/'.$frontendpage->post->featured_image)}}" class="attachment-full size-full wp-post-image" alt="" srcset="" sizes="(max-width: 800px) 100vw, 800px" />
                        </div>
                    <!-- /Post Image -->

                    <!-- Post Content -->
                    <div class="entry-content" style="text-align: justify">
                        {!! $frontendpage->post->content !!}
                    </div>
                    <!-- /Post Content -->

                </div>

                <div class="comments-area">
                    @if(Auth::check() && $frontendpage->post->comment_status=='open')
                        <div id="respond" class="comment-respond">
                            <h3 id="reply-title" class="comment-reply-title">Leave a Reply <small><a rel="nofollow" id="cancel-comment-reply-link" href="#" style="display:none;">Cancel reply</a></small></h3>
                            
                            <form action="{{route('comments.store')}}" method="post" id="commentform" class="comment-form">
                                @csrf
                                <p class="comment-notes">
                                    <span id="email-notes">Your email address will not be published.</span> Required fields are marked <span class="required">*</span>
                                </p>
                                <p class="comment-form-comment">
                                    <label for="comment">Comment</label> 
                                    <textarea id="content" name="content" cols="45" rows="8" maxlength="65525" required="required"></textarea>
                                </p>
                                <p class="comment-subscription-form">
                                    <input type="checkbox" name="subscribe_comments" id="subscribe_comments" value="subscribe" style="width: auto; -moz-appearance: checkbox; -webkit-appearance: checkbox;" /> 
                                    <label class="subscribe-label" id="subscribe-label" for="subscribe_comments">Notify me of follow-up comments by email.</label>
                                </p>
                                <p class="comment-subscription-form">
                                    <input type="checkbox" name="subscribe_blog" id="subscribe_blog" value="subscribe" style="width: auto; -moz-appearance: checkbox; -webkit-appearance: checkbox;" /> 
                                    <label class="subscribe-label" id="subscribe-blog-label" for="subscribe_blog">Notify me of new posts by email.</label>
                                </p>
                                <p class="form-submit">
                                    <input name="submit" type="submit" id="submit" class="submit" value="Post Comment" /> 
                                    <input type='hidden' name='post_id' value='{{$frontendpage->post->id}}' id='post_id' />
                                </p>
                            </form>
                        </div>
                    @endif
                    
                    <h3>{{ $frontendpage->post->comments()->where('status', 'Approved')->count() }} Comments</h3>
                    <!-- Comment list -->
                    @include('frontend/themes/'.$frontendpage->setting->themename.'/comments', ['comments'=>$frontendpage->post->comments()->where('status', 'Approved')->get(), 'post_id'=>$frontendpage->post->id])

                </div>
            </div>

            <!-- Sidebar -->
            <div class="col-md-3 sidebar">
                {{-- @if (file_exists('sidebar.blade.php')) --}}
                    <!--For Sidebar if sidebar exists -->
                    @include('frontend/themes/'.$frontendpage->setting->themename.'/sidebar')   
                {{-- @else
                    <p>Sidebar not found.</p>
                @endif --}}
            </div>

        </div>
    </div>
</div>