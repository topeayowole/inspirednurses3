<!-- Carousel -->
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        @foreach ($frontendpage->slides as $key=>$value)
            <li data-target="#carousel-example-generic" data-slide-to="{{$key}}" {!!$key==0?'class="active"':''!!}></li>
        @endforeach
    </ol>
      
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        @foreach($frontendpage->slides as $index=>$slide)
            <div class="item {{$index==0?'active':''}}">
                <img src="{{ asset('storage/'.$slide->image) }}" alt="">
                <div class="carousel-caption slidertext">
                    <h1 style="font-size:3vw; font-weight: normal">{{$slide->caption}}</h1>
                    <h3 style="font-size:3vw; font-weight: normal">{{$slide->subcaption}}</h3>
                </div>
            </div>
        @endforeach
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>