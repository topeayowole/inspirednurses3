@foreach ($comments as $comment)
    <!-- COMMENT 1 - START -->
    <div class="media">
        <a class="pull-left" href="#"><img class="media-object" src="{{asset('storage/'.$comment->user->photo)}}" alt=""></a>
        <div class="media-body">
            <h4 class="media-heading">{{ $comment->user->username }}</h4>
            <p>{{ $comment->content }}</p>
            <ul class="list-unstyled list-inline media-detail pull-left">
                <li><i class="fa fa-calendar"></i> {{ $comment->created_at->format('d/m/Y') }}</li>
                <li><i class="fa fa-thumbs-up"></i> 13</li>
            </ul>
            <ul class="list-unstyled list-inline media-detail pull-right">
                <li class=""><a href="">Like</a></li>
                <li class=""><a href="#" class="reply">Reply</a></li>
            </ul>
            <div style="clear:both;"></div>
            <div style="display:none;">
                <form action="{{route('comments.reply')}}" method="post">
                    @csrf
                    <input type='hidden' name='post_id' value='{{$post_id}}' id='post_id' />
                    <input type='hidden' name='comment_id' id='comment_id' value='{{$comment->id}}' />
                    <textarea name="content" class="form-control" id="" cols="2"></textarea>
                    <input type="submit" value="Post Reply" class="btn btn-info pull-right ">
                </form>
            </div>
            @include('frontend/themes/'.$frontendpage->setting->themename.'/comments', ['comments'=>$comment->replies()->where('status', 'Approved')->get()])
        </div>
    </div>
    <!-- COMMENT 1 - END -->
@endforeach

@section('scripts')
    <script>
        jQuery(document).ready(function(){
            jQuery('.reply').click(function(event){
                event.preventDefault();
                console.log('Hello World');
                // console.log(jQuery(this).parent());
                jQuery(this).parent().parent().next().next().slideToggle();
            });
        });
    </script>
@endsection


{{-- @foreach ($frontendpage->post->comments as $comment)
    <div class="row">
        <div class="col-md-2">
            @if(Storage::disk('public')->exists(Auth::user()->photo))
                <img width="50" src="{{asset('storage/'.Auth::user()->photo)}}" class="img-circle" alt="User Image" />
            @else
                <img width="50" src="{{asset('storage/users/avatar.png')}}" class="img-circle" alt="User Image" />
            @endif
        </div>
        <div class="col-md-10">
            <label for="" style="text-align: center">{{ Auth::user()->username }}</label> 
            <p>{{ $comment->content }}</p>
            <form action="{{route('comments.reply')}}" method="post">
                @csrf
                <textarea name="content" class="form-control" id="" cols="30" rows="10"></textarea>
                <input type='hidden' name='post_id' value='{{$frontendpage->post->id}}' id='post_id' />
                <input type='hidden' name='comment_id' id='comment_id' value='{{$comment->id}}' />
                <input type="submit" class="btn btn-primary" value="Reply" style="float:right">
            </form>
                
            
        </div>
    </div>
@endforeach --}}