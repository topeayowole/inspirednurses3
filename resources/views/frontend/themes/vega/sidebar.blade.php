
<div class="sidebar-widgets">
    <!-- Search -->
    <div id="search-3" class="widget widget_search">
        <h3 class="widget-title">Search</h3>
        <form role="search" method="get" class="search-form" action="#">
            <input type="search" class="search-field form-control" placeholder="Search..." value="" name="s" title="Search for:"  />
            <button class="btn btn-primary-custom" name="submit" type="submit"><i class="fa fa-arrow-right"></i></button>
        </form>
    </div>
    <!-- Search -->

    <!-- Recent Posts -->
    <div id="recent-posts-3" class="widget widget_recent_entries">
        <h3 class="widget-title">Recent Posts</h3>
        <ul>
            @forelse ($frontendpage->recentposts as $post)
                <li>
                    <a href="{{route('frontend.posts.show', $post->slug)}}">{{$post->title}}</a>
                </li>
            @empty
                <p>No recent posts.</p>
            @endforelse
        </ul>
    </div>

    <!-- Tags -->
    <div id="tag_cloud-1" class="widget widget_tag_cloud">
        <h3 class="widget-title">Tags</h3>
        <div class="tagcloud">
            {{-- <a href="#" class="tag-cloud-link tag-link-7 tag-link-position-1" style="font-size: 8pt;" aria-label="animal (2 items)">Animal</a> --}}
            @forelse ($frontendpage->tags as $tag)
                <a href="#" class="tag-cloud-link tag-link-7 tag-link-position-1" style="font-size: 8pt;" aria-label="animal (2 items)">{{$tag->name}}</a>
            @empty
                No tags.
            @endforelse
        </div>
    </div>
    <!-- Tags -->

    <!-- Text -->
    <div id="text-2" class="widget widget_text">
        <h3 class="widget-title">About {{$frontendpage->setting->sitename}}</h3>
        <div class="textwidget">
            {{ $frontendpage->setting->description }}
        </div>
    </div>
</div>
