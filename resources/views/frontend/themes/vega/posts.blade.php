<!-- ========== Banner - Custom Header ========== -->
<div class="jumbotron image-banner banner-custom-header" style="background:url('{{asset('themes/vega/images/cropped-country-road-1149667_1920-1.jpg')}}') no-repeat 0 0 #ffffff;background-size:cover;background-position:center center">
    <div class="container">
        <h1 class="block-title wow zoomIn">{{$frontendpage->name}}</h1>
    </div>
</div>
<!-- ========== /Banner - Custom Header ========== -->


<!-- ========== Content Starts ========== -->
<div class="section blog-feed bg-white">
    <div class="container">
        <div class="row">

            <div class="col-md-9 blog-feed-column">
                @forelse ($frontendpage->posts as $post)
                    <!-- Loop -->
                    <div id="post-293" class="entry clearfix wow zoomIn post-293 post type-post status-publish format-standard has-post-thumbnail sticky hentry category-sample-posts category-uncategorized">
                        
                        <!-- Post Title -->
                        <h3 class="entry-title block-title block-title-left">
                            <a href="{{route('frontend.posts.show', $post->slug)}}">
                                {{$post->title}}
                            </a>
                        </h3>

                        <!-- Large Image Top, Full Content Below -->
                        <div class="entry-image entry-image-top">
                            <a class="post-thumbnail post-thumbnail-large" href="{{route('frontend.posts.show', $post->slug)}}">
                                {{-- @if(Storage::disk('public')->exists($post->featured_image)) --}}
                                    <img width="1200" height="676" src="{{asset('storage/'.$post->featured_image)}}" class="attachment-full size-full wp-post-image" alt="Post image"  sizes="(max-width: 1200px) 100vw, 1200px" />
                                {{-- @else
                                    <img width="1200" height="676" src="{{asset('storage/post_images/default-post-image.jpg')}}" class="attachment-full size-full wp-post-image" alt="Post image"  sizes="(max-width: 1200px) 100vw, 1200px" />
                                @endif --}}
                            </a>
                        </div>

                         <div class="entry-content">
                            {!! Str::words($post->content, 90) !!}
                            
                            
                            @if (str_word_count($post->content) > 90)
                                <br/><br/> 
                                <a href="{{route('frontend.posts.show', $post->slug)}}" class="more-link">View full post&#8230;</a> 
                                <br/><br/>
                            @endif
                         </div>
                         <!-- /Large Image Top, Full Content Below -->

                         <!-- Post Meta -->
                        <div class="entry-meta ">
                            
                            Posted: {{$post->created_at->format('M d, Y')}} <br />
                            {{-- @if ($post->categories) --}}
                                Under:
                                @forelse ($post->categories as $category)
                                    <a href="{{route('frontend.category.posts', $category->slug)}}">{{$category->name}}</a>, 
                                @empty
                                    Uncategorised
                                @endforelse
                                <span class="sep">/</span>
                                <i class="ion ion-ios-eye"></i> {{ $post->postViews()->count() }}
                            {{-- @endif --}}
                        </div>
                        <!-- /Post Meta -->

                    </div>
                    <!-- /Post -->

                @empty
                    <p>No Post</p>
                @endforelse
                
                <!-- Pagination -->
                {{ $frontendpage->posts->links() }}
                <!-- /Pagination -->
            </div>

            <!-- Sidebar -->
            <div class="col-md-3 sidebar">
                @include('frontend/themes/'.$frontendpage->setting->themename.'/sidebar')
            </div>
            
        </div>
    </div>
</div>