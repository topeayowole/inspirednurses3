<div class="footer-widgets bg-footer">
    <div class="container">
        <div class="row">

            <!-- Footer Col 1 -->
            <div class="col-md-3 footer-widget footer-widget-col-1 ">
                <div id="text-2" class="widget widget_text">
                    <h3 class="widget-title">About {{$frontendpage->setting->sitename}}</h3>
                    <div class="textwidget">
                        {{ $frontendpage->setting->description }}  <br /><br />
                        {{-- Inspired Nurses Network, Africa (I.N.N.A), popularly known as the Inspire Nurses Network is an evolving non-for-profit, 
                        non-political and non-partisan nursing organization comprising of passionate, dedicated and selfless volunteer 
                        professional nurses based in Nigeria and overseas. <br> <br> --}}
                        <a href="https://www.facebook.com/Inspired-Nurses-Africa-1462090660784229/" class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook"></i></a>
                        <a href="https://www.twitter.com/inspired_nurses" class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></a>
                        <a href="https://www.tinyurl.com/INNAWhatsappContact/" class="btn btn-social-icon btn-flickr" style="background: #25d366"><i class="fa fa-whatsapp"></i></a>
                        {{-- <a href="https://www.youtube.com" class="btn btn-social-icon btn-google"><i class="fa fa-youtube"></i></a> --}}
                    </div>
                </div>
            </div>
            <!-- /Footer Col 1 -->

            <!-- Footer Col 2 -->
            <div class="col-md-3 footer-widget footer-widget-col-2 ">
                <div id="categories-3" class="widget widget_categories">
                    <h3 class="widget-title">Recent Posts</h3>
                    <ul>
                        @forelse ($frontendpage->recentposts as $post)
                            <li class="cat-item cat-item-6"><a href="{{route('frontend.posts.show', $post->slug)}}">{{$post->title}}</a></li>
                        @empty
                            <p>No recent post.</p>
                        @endforelse
                        {{-- <li class="cat-item cat-item-1"><a href="#">Bedrooms</a></li>
                        <li class="cat-item cat-item-1"><a href="#">Kitchens</a></li>
                        <li class="cat-item cat-item-1"><a href="#">Offices</a></li>
                        <li class="cat-item cat-item-1"><a href="#">Dinning Rooms</a></li>
                        <li class="cat-item cat-item-1"><a href="#">Security Doors</a></li> --}}
                    </ul>
                </div>
            </div>
            <!-- /Footer Col 2 -->

            <!-- Footer Col 3 -->
            <div class="col-md-3 footer-widget footer-widget-col-3 ">
                <div id="text-3" class="widget widget_text">
                    <h3 class="widget-title">Contact us</h3>
                    <div class="textwidget">
                        <ul class="icon-list">
                            <li><i class="fa fa-home"></i>No 14, Muritala Eletu Street, Osapa London, Behind Sebuff Plaza, Lekki, Lagos.</li>
                            <li><i class="fa fa-phone"></i>
                                <a href="tel:+2347017333732">+234 701 733 3732</a>
                                <a href="tel:+2348068569227">+234 806 856 9227</a>
                                <a href="tel:+2348060965538">+234 806 096 5538</a>
                            </li>
                            <li><i class="fa fa-paper-plane"></i><a href="mailto:admin@inspirenurses.org">admin@inspirenurses.org</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /Footer Col 3 -->

            <!-- Footer Col 4 -->
            <div class="col-md-3 footer-widget footer-widget-col-4 ">
                <div id="facebook-likebox-1" class="widget widget_facebook_likebox">
                    <h3 class="widget-title">
                        <a href="https://www.facebook.com/Inspired-Nurses-Africa-1462090660784229/">Connect With Us</a>
                    </h3>
                    <div id="fb-root"></div>
                    <div class="fb-page" data-href="https://www.facebook.com/Inspired-Nurses-Africa-1462090660784229/" data-width="340" data-height="130" data-hide-cover="false" data-show-facepile="false" data-show-posts="false">
                        <div class="fb-xfbml-parse-ignore">
                            <blockquote cite="https://www.facebook.com/Inspired-Nurses-Africa-1462090660784229/">
                                <a href="https://www.facebook.com/Inspired-Nurses-Africa-1462090660784229/">Like Us</a>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Footer Col 4 -->

        </div>
    </div>
</div><div class="footer">
    <div class="container">
        <div class="row">

            <!-- Navigation -->
            <div class="col-md-8">
                <ul id="menu-footer" class="nav-foot">
                    {{-- <li class="menu-item menu-item-type-custom menu-item-object-custom">
                        <a href="#">Contact</a>
                    </li>
                    <li class="menu-item menu-item-type-custom menu-item-object-custom">
                        <a href="#">Terms</a>
                    </li>
                    <li class="menu-item menu-item-type-custom menu-item-object-custom">
                        <a href="#">Privacy</a>
                    </li> --}}
                </ul>
            </div>

            <!-- Copyright and Credits -->
            <div class="col-md-4">
                <div class="copyright">
                    Copyright &copy; {{ date('Y') }} {{$frontendpage->setting->sitename}} 
                </div>
            </div>

        </div>
    </div>
</div><div id="back_to_top">
    <div class="container">
        <a href="#"><i class="fa fa-chevron-up"></i></a>
    </div>
</div>

<script type="text/javascript" src="{{asset('themes/vega/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/vega/js/jquery-migrate.min.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/vega/js/jquery.form.min.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/vega/js/jquery.blockUI.min.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/vega/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/vega/js/jquery.smartmenus.min.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/vega/js/jquery.smartmenus.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/vega/js/wow.min.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/vega/js/vega-wp-anim.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/vega/js/parallax.min.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/vega/js/vega-wp.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/vega/js/jquery.prettyPhoto.js')}}"></script>
<script src="{{asset('themes/vega/js/blueimp-gallery.min.js')}}"></script>

@yield('scripts')

<script type="text/javascript">
    var jpfbembed = {"appid":"249643311490", "locale":"en_US"};
</script>

<script type="text/javascript" src="{{asset('themes/vega/js/jetpack/facebook-embed.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/vega/js/wp-embed.min.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/vega/js/jquery-validation/dist/jquery.validate.min.js')}}"></script>


<!-- Jssor Gallery Slider-->
<script type="text/javascript" src="{{asset('images/gallery/js/jssor.slider-24.1.5.min.js')}}"></script>
<script type="text/javascript" src="{{asset('images/gallery/js/gallery.js')}}"></script>

{{-- For page specific Javascript --}}
<script>
    @if(isset($frontendpage->page->javascript))
        {!! $frontendpage->page->javascript !!}
    @endif
</script>

{{-- For global javascript --}}
<script>
    {!! $frontendpage->setting->javascript !!}
</script>
	
</body>
</html>