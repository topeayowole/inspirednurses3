<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="{{asset('themes/vega/css/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/vega/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/vega/css/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/vega/css/bootstrap-social.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/vega/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/vega/css/jetpack.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/vega/css/custom.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/vega/css/orange.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/vega/css/blueimp-gallery.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/vega/css/Ionicons/css/ionicons.min.css')}}">

    <!--Jsor Gallery css -->
    <link rel="stylesheet" type="text/css" href="{{asset('images/gallery/css/gallerystyle.css')}}">
    
    {{-- For page specific styles --}}
    <style>
        @if(isset($frontendpage->page->styles))
            {!! $frontendpage->page->styles !!}
        @endif
    </style>

    {{-- For global styles --}}
    <style>
        {!! $frontendpage->setting->styles !!}
    </style>

    {{-- <title>{{$frontendpage->name==null ? $frontendpage->name.' | ' : ''}} {{$frontendpage->setting->sitename}}</title> --}}
    <title>{{$frontendpage->name}} | {{$frontendpage->setting->sitename}}</title>
</head>

<body class="home page page-home">
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 header-left">
                    <ul id="menu-top-left" class="top-menu">
                        <li class="menu-item menu-item-type-custom menu-item-object-custom">
                            <a href="mailto:admin@inspirenurses.org"><i class="fa fa-envelope" aria-hidden="true"></i> admin@inspirenurses.org</a>
                        </li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom">
                            <a href="tel:+2347017333732"><i class="fa fa-phone" aria-hidden="true"></i> +234 701 733 3732</a>
                        </li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom">
                            <a href="tel:+2348068569227"><i class="fa fa-phone" aria-hidden="true"></i> +234 806 856 9227</a>
                        </li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom">
                            <a href="tel:+2348060965538"><i class="fa fa-phone" aria-hidden="true"></i> +234 806 096 5538</a>
                        </li>
                    </ul>
                </div>

                <div class="col-sm-5 header-right">
                    <ul id="menu-top-right" class="social-menu top-menu">
                        <li class="menu-item menu-item-type-custom menu-item-object-custom">
                            <a href="https://www.facebook.com/Inspired-Nurses-Africa-1462090660784229/" target="_blank" rel="noopener noreferrer">Facebook</a>
                        </li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom">
                            <a href="https://www.twitter.com/inspired_nurses" target="_blank" rel="noopener noreferrer">Twitter</a>
                        </li>
                        <li class="menu-item menu-item-type-custom menu-item-object-custom" style="text-align: center">
                            <a href="https://www.tinyurl.com/INNAWhatsappContact/" target="_blank" rel="noopener noreferrer"><i class="fa fa-whatsapp"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
</div>
<div class="header-toggle"><i class="fa fa-angle-down"></i></div><div class="nav-wrapper">
    <div class="navbar navbar-custom" role="navigation">
        <div class="container">
            <!-- Logo -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><i class="fa fa-bars"></i></button>
                <a class="navbar-brand text-logo" href="{{url('/')}}"><img src="{{asset('storage/settings/logo.jpg') }}" width="50" alt="Logo"></a>
            </div>
            <!-- /Logo -->

            <!-- Navigation -->
            <div class="navbar-collapse collapse">
                <ul id="menu-header" class="nav navbar-nav navbar-right menu-header">
                    @foreach ($frontendpage->menus as $menu)
                        <li class="{{ $menu->children->count() ? 'menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children nav-item dropdown' :  'page-scroll menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item active'}}">
                            {{-- <a {!! @if(isset($menuname)) $menu->name==$menuname?"class='showing'": "" @endif !!} title="{{$menu->name}}" href="{{url($menu->slug)}}"> {{$menu->name}}</a> --}}
                            <!-- Two nested - if shortcuts -->
                            <a {!! isset($frontendpage->name) ? ($menu->name==$frontendpage->name?"class='showing'": "") : '' !!} href="{{url($menu->slug)}}" {{$menu->children->count() ? "data-toggle='dropdown' class='dropdown-toggle'": ""}}> {{$menu->name}} {!! $menu->children->count() ? "<b class='fa fa-angle-down'></b>": "" !!}</a>
                            @if ($menu->children->count())
                                @include('frontend/themes/'.$frontendpage->setting->themename.'/submenu', ['children'=>$menu->children()->orderBy('order')->get()])
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
