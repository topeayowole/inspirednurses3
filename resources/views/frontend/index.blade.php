@include('frontend/themes/'.$frontendpage->setting->themename.'/header')

{{-- <div class="container"> --}}
@if ($frontendpage->page != null)
    @include('frontend/themes/'.$frontendpage->setting->themename.'/page')
@endif

@if ($frontendpage->post != null)
    @include('frontend/themes/'.$frontendpage->setting->themename.'/single-post')
@endif

@if ($frontendpage->posts)
    @include('frontend/themes/'.$frontendpage->setting->themename.'/posts')
@endif
{{-- </div> --}}


@include('frontend/themes/'.$frontendpage->setting->themename.'/footer')