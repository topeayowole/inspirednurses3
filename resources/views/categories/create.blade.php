@extends('layouts.dashboard')

@section('content')
    @include('partials.successmsg')
    @include('partials.errorbag')
    <section class="content-header">
        <h1>Add New Post Category</h1> 
    </section>

    {{-- Main content --}}
    <section class="content">
        <div class="row">
            <div class="col-md-5">
                <div class="box">
                    <div class="box-header">
                        <div class="box-title">Add new category</div>
                    </div>
                    <div class="box-body">
                        <form action="{{ route('categories.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" id="" class="form-control">
                            </div>
                
                            <div class="form-group">
                                <label>Parent category</label>
                                <select name="parent_id" id="" class="form-control">
                                    <option value="0">None</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Add category image</label>
                                <input type="file" name="image" id="">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-info">Add Category</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection