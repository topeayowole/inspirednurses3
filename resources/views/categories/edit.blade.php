@extends('layouts.dashboard')

@section('content')
    @include('partials.successmsg')
    @include('partials.errorbag')
    <section class="content-header">
        <h1>Edit category <a href="{{route('categories.index')}}" class="btn btn-info">All Categories</a></h1> 
    </section>

    {{-- Main content --}}
    <section class="content">
        <div class="row">
            <div class="col-md-5">
                <div class="box">
                    <div class="box-header">
                        <div class="box-title">Edit category</div>
                    </div>
                    <div class="box-body">
                        <form action="{{ route('categories.update', $category->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" value="{{ $category->name }}" class="form-control">
                            </div>
                
                            <div class="form-group">
                                <label>Parent category</label>
                                <select name="parent_id" id="" class="form-control">
                                    <option value="0">None</option>
                                    @foreach ($categories as $singlecategory)
                                        <option value="{{ $singlecategory->id }}" {{$singlecategory->id==$category->parent_id ? 'selected':''}}>{{ $singlecategory->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Category image</label>
                                <input type="file" name="image" id="">
                                <img src="{{ asset('storage/'.$category->image) }}" width="100" class="img-rounded">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-info">Update Category</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection