@extends('layouts.dashboard')

@section('content')
    @include('partials.successmsg')
    <section class="content-header">
        <h1>Post Categories <a href="{{route('categories.create')}}" class="btn btn-info">Add New</a></h1> 
    </section>

    {{-- Main content --}}
    <section class="content">
        <div class="row">
            <div class="col-md-7">
                <div class="box box-primary">
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" name="" id=""></th>
                                    <th><i class="fa fa-picture-o"></i></th>
                                    <th>Name</th>
                                    <th>Parent Category</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $category)
                                    <tr>
                                        <td><input type="checkbox" name="" id=""></td>
                                        <td><img src="{{ asset('storage/'.$category->image) }}" width="50"></td>
                                        <td>
                                            <a href="{{ route('categories.edit', $category->id) }}" title="Click to edit category">{{ $category->name }}</a> <br> 
                                        </td>
                                        <td>{{ $category->parentcategory->name ?? 'None' }}</td>
                                        <td><a href="{{ route('categories.edit', $category->id) }}" class="btn btn-success"><i class="fa fa-pencil"></i></a></td>
                                        <td>
                                            <form id="deleteForm" method="post" action="{{ route('categories.destroy', $category->id) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger" onClick='return confirm("Are you sure you want to delete?")'><i class="fa fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection