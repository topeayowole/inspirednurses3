@extends('layouts.dashboard')

@section('content')
    @include('partials.successmsg')
    @include('partials.errorbag')
    <section class="content-header">
        <h1>Add New Menu</h1> 
    </section>

    {{-- Main content --}}
    <section class="content">
        <div class="row">
            <div class="col-md-5">
                <div class="box">
                    <div class="box-header">
                        <div class="box-title">Add new menu</div>
                    </div>
                    <div class="box-body">
                        <form action="{{ route('menus.store') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" id="" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Parent Menu</label>
                                <select name="parent_id" class="form-control">
                                    <option value="">None</option>
                                    @foreach ($menus as $menu)
                                        <option value="{{$menu->id}}">{{$menu->name}}</option>   
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Order</label>
                                <input type="number" name="order" value="1" class="form-control" min="1">
                            </div>

                            <div class="form-group">
                                <label>Menu Type</label>
                                <select name="menu_type" class="form-control" v-model="menu_type" v-on:change="getfromdb">
                                    <option value="">---Select---</option>
                                    <option value="page">Page</option>
                                    <option value="post">Single Post</option>
                                    <option value="post category">A Post Category</option>
                                    <option value="all posts">All Posts</option>
                                    <option value="external url">External URL</option>
                                </select>
                            </div>

                            <div class="form-group" v-if="menu_type=='page'||menu_type=='post'||menu_type=='post category'">
                                <label>Select @{{menu_type}}</label>
                                <select name="menu_target_id" class="form-control">
                                    <option v-bind:value="option.id" v-for="option in options">@{{option.title}}</option>
                                </select>
                            </div>

                            <div class="form-group" v-if="menu_type=='external url'">
                                <label>External URL</label>
                                <input type="text" name="external_url" id="" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="">Set as Homepage: </label>
                                <input type="radio" name="is_homepage" value="1"> Yes &nbsp;
                                <input type="radio" name="is_homepage" value="0" checked> No
                            </div>
                
                            <div class="form-group">
                                <button type="submit" class="btn btn-info">Add Menu</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        new Vue({
            el: '#app',
            data: {
                menu_type : '',
                options: [],
            },
            methods: {
                getfromdb: function () {
                    axios.get("{{url('api/getdatafromdb?type=')}}"+this.menu_type)
                    .then(response=>{
                        console.log(response.data);
                        this.options = response.data;
                    });
                }
            }
        });
    </script>
@endsection