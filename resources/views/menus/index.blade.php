@extends('layouts.dashboard')

@section('content')
    @include('partials.successmsg')
    <section class="content-header">
        <h1>Menus <a href="{{route('menus.create')}}" class="btn btn-info">Add New</a></h1> 
    </section>

    {{-- Main content --}}
    <section class="content">
        <div class="row">
            <div class="col-md-7">
                <div class="box box-primary">
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" name="" id=""></th>
                                    <th>Name</th>
                                    <th>Parent Menu</th>
                                    <th>Order</th>
                                    <th>Home</th>
                                    <th>Menu Type</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($menus as $menu)
                                    <tr>
                                        <td><input type="checkbox" name="" id=""></td>
                                        <td>{{$menu->name}}</td>
                                        <td>{{$menu->parent==null ? 'None' : $menu->parent->name}}</td>
                                        <td>{{ $menu->order}}</td>
                                        <td>{!! $menu->is_homepage ? "<i class='fa fa-home' style='color:grey'></i>" : '' !!}</td>
                                        <td>{{ ucfirst($menu->menu_type)}}</td>
                                        <td><a href="{{ route('menus.edit', $menu->id) }}" class="btn btn-success"><i class="fa fa-pencil"></i></a></td>
                                        <td>
                                            <form id="deleteForm" method="post" action="{{ route('menus.destroy', $menu->id) }}">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger" onClick='return confirm("Are you sure you want to delete?")'><i class="fa fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection