@extends('layouts.dashboard')

@section('styles')
    @include('partials.datatableStyles')

    <style>
        .pagination-area{
            display: flex;
            justify-content: center;
        }
    </style>
@endsection

@section('content')
    @include('partials.successmsg')
    <section class="content-header">
        <h1>Contacts</h1> 
    </section>

    {{-- Main content --}}
    {{-- <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Contacts</h3>

                        <div class="box-tools pull-right">
                            <div class="has-feedback">
                                <input type="text" class="form-control input-sm" placeholder="Search">
                                <span class="glyphicon glyphicon-search form-control-feedback"></span>
                            </div>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        <div class="mailbox-controls">
                            <!-- Check all button -->
                            <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                            </button>

                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                            </div>
                            <!-- /.btn-group -->

                            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                            <div class="pull-right">
                                1-50/200
                                
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                                </div>
                                <!-- /.btn-group -->
                            </div>
                            <!-- /.pull-right -->
                        </div>
                        
                        <div class="table-responsive mailbox-messages">
                            <table id="example1" class="table table-hover table-striped">
                                <tbody>
                                    @foreach ($contacts as $contactus)
                                        <tr>
                                            <td><input type="checkbox" name="" id=""></td>
                                            <td  class="mailbox-name">
                                                <a href="{{ route('contactus.show', ['id' => $contactus->id]) }}">{{ $contactus->name }}</a>
                                            </td>
                                            <td class="mailbox-subject"><b>{{ Str::words($contactus->subject, 3) }}</b> - {{ Str::words($contactus->message, 8) }}</td>
                                            <td>{{ $contactus->created_at->format('d M. Y g:i A') }} </td>
                                            <td>{{ $contactus->email }}</td>
                                            <td>{{ $contactus->phone }}</td>
                                            <td></td>
                                            <td>
                                                <a href="{{ route('contactus.show', $contactus->id) }}" class="btn btn-success">View</a>
                                            </td>
                                            <td>
                                                <form id="deleteForm" method="post" action="{{ route('contactus.destroy', $contactus->id) }}">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger" onClick='return confirm("Are you sure you want to delete?")'>Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <div class="pagination-area">
                    {{$contacts->links()}}
                </div>
            </div>
            <!-- /.col-md-12 -->        
        </div>
        <!-- /.row -->

    </section> --}}

    <section class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-responsive">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="" id=""></th>
                                <th>Name</th>
                                <th>Subject</th>
                                <th>Date</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contacts as $contactus)
                                <tr>
                                    <td><input type="checkbox" name="" id=""></td>
                                    <td  class="mailbox-name">
                                        <a href="{{ route('contactus.show', ['id' => $contactus->id]) }}">{{ $contactus->name }}</a>
                                    </td>
                                    <td class="mailbox-subject"><b>{{ Str::words($contactus->subject, 3) }}</b> - {{ Str::words($contactus->message, 8) }}</td>
                                    <td>{{ $contactus->created_at->format('d M. Y g:i A') }} </td>
                                    <td>{{ $contactus->email }}</td>
                                    <td>{{ $contactus->phone }}</td>
                                    <td>
                                        <a href="{{ route('contactus.show', $contactus->id) }}" class="btn btn-success">View</a>
                                    </td>
                                    <td>
                                        <form id="deleteForm" method="post" action="{{ route('contactus.destroy', $contactus->id) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger" onClick='return confirm("Are you sure you want to delete?")'>Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    @include('partials.datatableScripts')
@endsection
