@extends('layouts.dashboard')

@section('content')
    <section class="content-header">
        <h1>Contact Us </h1>
    </section>

    {{-- Main content --}}
    <section class="content">
        <div class="row">
            <div class="col-md-9">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Read Contacts</h3>

                        <div class="box-tools pull-right">
                            @if (!is_null($previous))
                                <a href="{{ route('contactus.show', $previous->id) }}" class="btn btn-box-tool" data-toggle="tooltip" title="Previous"><i class="fa fa-chevron-left"></i></a>
                            @endif

                            @if (!is_null($next))
                                <a href="{{ route('contactus.show', $next->id) }}" class="btn btn-box-tool" data-toggle="tooltip" title="Next"><i class="fa fa-chevron-right"></i></a>
                            @endif
                        </div>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body no-padding">
                         <div class="mailbox-read-info">
                             <h3>{{ $contactus->subject }}</h3>
                             <h5> {{ $contactus->name }}</h5>
                             <h5> {{ $contactus->phone }}</h5>
                             <h5>From: {{ $contactus->email }}
                                <span class="mailbox-read-time pull-right">{{ $contactus->created_at->format('d M. Y g:i A') }}</span>
                            </h5>
                         </div>
                         <!-- /.mailbox-read-info -->
                        
                        <div class="mailbox-controls with-border text-center">
                            <div class="btn-group">
                            <a type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Delete" onclick="event.preventDefault(); if(confirm('Are you sure you want to delete?')){document.getElementById('delete-form').submit();}">
                                <i class="fa fa-trash-o"></i>
                            </a>
                            <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Reply">
                                <i class="fa fa-reply"></i>
                            </button>
                            <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Forward">
                                <i class="fa fa-share"></i>
                            </button>
                            </div>
                            <!-- /.btn-group -->
                            <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" title="Print">
                                <i class="fa fa-print"></i>
                            </button>
                        </div>
                        <!-- /.mailbox-controls -->

                        <div class="mailbox-read-message">
                            {{ $contactus->message }}
                        </div>
                        <!-- /.mailbox-read-message -->
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <div class="pull-right">
                            {{-- <a type="button" class="btn btn-default" href="{{route('contactus.reply', $contactus->id)}}"><i class="fa fa-reply"></i> Reply</a> --}}
                            {{-- <button type="button" class="btn btn-default"><i class="fa fa-share"></i> Forward</button> --}}
                        </div>
                        <a type="button" class="btn btn-default" onclick="event.preventDefault(); if(confirm('Are you sure you want to delete?')){document.getElementById('delete-form').submit();}"><i class="fa fa-trash-o"></i> Delete</a>
                        <button type="button" class="btn btn-default"><i class="fa fa-print"></i> Print</button>
                    </div>
                    <!-- /.box-footer -->

                </div>
                <!-- /. box -->

            </div>
            <!-- /.col-md-9 -->

        </div>
        <!-- /.row -->
    </section>

    <form id="delete-form" action="{{ route('contactus.destroy', $contactus->id) }}" method="post">
        @csrf
        @method('DELETE')               
    </form>
@endsection