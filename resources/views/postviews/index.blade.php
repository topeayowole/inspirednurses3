@extends('layouts.dashboard')

@section('styles')
    @include('partials.datatableStyles')
@endsection

@section('content')
    @include('partials.successmsg')
    <section class="content-header">
        <h1>Post Views 
        
            <form method="post" action="{{route('postviews.deleteall')}}" class="pull-right">
                @csrf
                <input type="hidden" name="_method" value="DELETE">
                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you want you delete all records?')">Delete All</button>
            </form>
        </h1> 
    </section>

    {{-- Main content --}}
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-responsive">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="" id=""></th>
                                <th>S/N</th>
                                <th>Post</th>
                                <th>Session</th>
                                <th>User</th>
                                <th>IP Address</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Agent</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($postviews as $postview)
                                <tr>
                                    <td><input type="checkbox" name="" id=""></td>
                                    <td>{{ $postview->id }}</td>
                                    <td><a href="{{ route('posts.edit', $postview->post->id) }}">{{ $postview->post->title }}</a></td>
                                    <td>{{ $postview->session_id }}</td>
                                    <td>{{ $postview->user_id==null ? 'visitor' : $postview->user->username }}</td>
                                    <td>{{ $postview->ip }}</td>
                                    <td>{{ $postview->country }}</td>
                                    <td>{{ $postview->city }}</td>
                                    <td>{{ $postview->agent }}</td>
                                    <td>
                                        <form id="deleteForm" method="post" action="{{ route('postviews.destroy', $postview->id) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger" onClick='return confirm("Are you sure you want to delete?")'><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                             @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    
@endsection

@section('scripts')
    @include('partials.datatableScripts')
@endsection