@extends('layouts.dashboard')

@section('content')
    @include('partials.errorbag')
    @include('partials.successmsg')

    <section class="content-header">
        <h1>Add New User</h1>
    </section>

    {{-- Main content --}}
    <section class="content">
        <div class="box box-info">
            <div class="box-header">
                <div class="box-title">Add New User</div>
            </div>
            <div class="box-body">
                <form action="{{ route('users.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="">Firstname</label>
                                <input type="text" name="firstname" class="form-control" value="{{ old('firstname') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="">Lastname</label>
                                <input type="text" name="lastname" class="form-control" value="{{ old('lastname') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" name="email" class="form-control" value="{{ old('email') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="">Username</label>
                                <input type="text" name="username" class="form-control" value="{{ old('username') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="">Role</label>
                                <select name="role" class="form-control">
                                    <option value="Admin" {{ old('role')=='Admin' ? 'selected' : '' }}>Admin</option>
                                    <option value="Writer" {{ old('role')=='Writer' ? 'selected' : '' }}>Writer</option>
                                    <option value="Subscriber" {{ old('role')=='Subscriber' ? 'selected' : '' }}>Subscriber</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Phone</label>
                                <input type="text" name="phone" class="form-control" value="{{ old('phone') }}" required maxlength="11">
                            </div>
                            <div class="form-group">
                                <label for="">Password</label>
                                <input type="password" name="password" class="form-control" value="{{ old('password') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="">Confirm Password</label>
                                <input type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-1">

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Profile Photo</label>
                                <input type="file" name="photo" id="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success">Create</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection