@extends('layouts.dashboard')

@section('content')
    @include('partials.errorbag')
    @include('partials.successmsg')

    <section class="content-header">
        <h1>Change Password</h1>
    </section>

    {{-- Main content --}}
    <section class="content">
        <div class="box box-info">
            <div class="box-header">
                <div class="box-title">Change Password</div>
            </div>
            <div class="box-body">
                <form action="{{ route('users.changePassword', $user->id) }}" method="post">
                    @csrf
                    @method('PATCH')
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="">Old Password</label>
                                <input type="password" name="oldpassword" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="">New Password</label>
                                <input type="password" name="password" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="">Confirm Password</label>
                                <input type="password" name="password_confirmation" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Change Password</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection