@extends('layouts.dashboard')

@section('content')
    @include('partials.errorbag')
    @include('partials.successmsg')

    <section class="content-header">
        <h1>User Profile</h1>
    </section>

    {{-- Main content --}}
    <section class="content">
        <div class="box box-info">
            <div class="box-header">
                <div class="box-title">User Profile</div>
            </div>
            <div class="box-body">
                <form action="{{ route('users.updateProfile', $user->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="">Firstname</label>
                                <input type="text" name="firstname" class="form-control" value="{{old('firstname', $user->firstname)}}">
                            </div>
                            <div class="form-group">
                                <label for="">Lastname</label>
                                <input type="text" name="lastname" class="form-control" value="{{old('lastname', $user->lastname)}}">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" name="email" class="form-control" value="{{old('email', $user->email)}}">
                            </div>
                            <div class="form-group">
                                <label for="">Username</label>
                                <input type="text" name="username" class="form-control" value="{{old('username', $user->username)}}">
                            </div>
                            <div class="form-group">
                                <label for="">Role</label>
                                <select name="role" class="form-control">
                                    <option value="Admin" {{ $user->role=='Admin' ? 'selected' : '' }}>Admin</option>
                                    <option value="Writer" {{ $user->role=='Writer' ? 'selected' : '' }}>Writer</option>
                                    <option value="Subscriber" {{ $user->role=='Subscriber' ? 'selected' : '' }}>Subscriber</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Phone</label>
                                <input type="text" name="phone" class="form-control" value="{{old('phone', $user->phone)}}">
                            </div>
                        </div>
                        <div class="col-md-1">

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <img src="{{asset('storage/'.$user->photo)}}" alt="user image" class="img-responsive img-circle">
                            </div>
                            <div class="form-group">
                                <label for="">Member since {{ $user->created_at->format('jS \o\f\ F, Y') }}</label>
                            </div>
                            <div class="form-group">
                                <label for="">Change Profile Picture</label>
                                <input type="file" name="photo" id="">
                            </div>
                            <div class="form-group">
                                <a href="{{ route('users.changePassword', $user->id) }}" class="btn btn-primary">Change Password</a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success">Update Profile</button>
                        </div>
                    </div>
                </form>
            </div>    
        </div>
    </section>
@endsection