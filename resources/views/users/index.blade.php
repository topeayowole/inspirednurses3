@extends('layouts.dashboard')

@section('styles')
    @include('partials.datatableStyles')
@endsection

@section('content')
    @include('partials.successmsg')
    <section class="content-header">
        <h1>Users <a href="{{route('users.create')}}" class="btn btn-info">Add New</a></h1> 
    </section>

    {{-- Main content --}}
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-responsive">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="" id=""></th>
                                <th>Photo</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Firstname</th>
                                <th>Lastname</th>
                                <th>Phone</th>
                                <th>Role</th>
                                <th>Date Joined</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($users as $user)
                                <tr>
                                    <td><input type="checkbox" name="" id=""></td>
                                    <td><img src="{{asset('storage/'. $user->photo)}}" alt="user photo" width="50" class="img-circle"></td>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->firstname }}</td>
                                    <td>{{ $user->lastname }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>{{ $user->role }}</td>
                                    <td>{{ $user->created_at->format('j F, Y') }}</td>
                                    <td>
                                        <a href="{{ route('users.edit', $user->id) }}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                    </td>
                                    <td>
                                        <form id="deleteForm" method="post" action="{{ route('users.destroy', $user->id) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger" onClick='return confirm("Are you sure you want to delete?")'><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')
    @include('partials.datatableScripts')
@endsection