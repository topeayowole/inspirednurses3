@extends('layouts.dashboard')

@section('styles')
    @include('partials.datatableStyles')
@endsection

@section('content')
    @include('partials.successmsg')
    <section class="content-header">
        <h1>Pages <a href="{{route('pages.create')}}" class="btn btn-info">Add New</a></h1> 
    </section>

    {{-- Main content --}}
    <section class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-responsive">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="" id=""></th>
                                <th>Title</th>
                                <th>Author</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pages as $page)
                                <tr>
                                    <td><input type="checkbox" name="" id=""></td>
                                    <td>
                                        <a href="{{ route('pages.edit', $page->id) }}">{{ $page->title }}</a> 
                                    </td>
                                    <td>{{ $page->user->firstname ?? $page->user->username }} {{ $page->user->lastname }}</td>
                                    <td>{{ ucfirst($page->status) }}</td>
                                    <td>Published {{ $page->created_at->diffForHumans() }}</td>
                                    <td>
                                        <a href="{{ route('pages.edit', $page->id) }}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                    </td>
                                    <td>
                                        <form id="deleteForm" method="post" action="{{ route('pages.destroy', $page->id) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger" onClick='return confirm("Are you sure you want to delete this page?")'><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    @include('partials.datatableScripts')
@endsection
