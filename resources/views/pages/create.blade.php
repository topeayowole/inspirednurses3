@extends('layouts.dashboard')

@section('styles')
    <link href="{{asset('summernote/dist/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
@endsection

@section('content')
    @include('partials.errorbag')
    @include('partials.successmsg')

    <section class="content-header">
        <h1>Add New Page</h1>
    </section>

    {{-- Main content --}}
    <section class="content">
        <form action="{{ route('pages.store') }}" method="post">
            @csrf
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="">Page Title</label>
                        <input type="text" name="title" id="" class="form-control" required placeholder="Title">
                    </div>
                    <div class="form-group">
                        <label for="">SEO Description (optional)</label>
                        <input type="text" name="seo_description" id="" class="form-control" placeholder="SEO Description (optional)">
                    </div>
                    <div class="form-group">
                        <label for="">Page Content</label>
                        <textarea name="content" class="summernote" id="summernote"></textarea>
                    </div>
                    <br>
                    <p style="font-style: italic">For Developers...</p>
                    <div class="form-group">
                        <label for="">Style Sheet</label> <br>
                        <textarea name="styles" id="styles" cols="110" rows="30"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="">JavaScript</label> <br>
                        <textarea name="javascript" id="javascript" cols="110" rows="30"></textarea>
                    </div>
                </div>

                {{-- Second column --}}
                <div class="col-md-4">
                    <div class="box box-info">
                        <div class="box-header">
                            <h4>Publish</h4>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <button class="btn btn-default">Save Draft</button>
                                <button class="btn btn-default pull-right">Preview</button>
                            </div>
                            <br>

                            <div class="form-group">
                                <i class="fa fa-info-circle"></i> <label for="">Status: </label>
                                <select name="status" id="" class="form-control">
                                    <option value="draft">Draft</option>
                                    <option value="published">Published</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <i class="fa fa-eye"></i> <label for="">Visibility: </label>
                                <select name="visibility" id="" class="form-control">
                                    <option value="private">Private</option>
                                    <option value="public">Public</option>
                                    <option value="published">Published</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="box-footer">
                            <div class="form-group">
                                <button type="submit" class="btn btn-info pull-right">Publish</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>      
        </form>
    </section>
@endsection

@section('scripts')
    <script src="{{asset('summernote/dist/summernote.js')}}"></script>
    <script src="{{asset('js/moment.js')}}"></script>
    <script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                height:450,
                placeholder: 'Page content'
            });
        });
    </script>
@endsection