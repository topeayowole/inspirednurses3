@extends('layouts.dashboard')

@section('styles')
    <link href="{{asset('summernote/dist/summernote.css')}}" rel="stylesheet">
@endsection


@section('content')
    @include('partials.errorbag')
    @include('partials.successmsg')
    <section class="content-header">
        <h1>Edit page</h1>
    </section>

    {{-- Main content --}}
    <section class="content">
        <form action="{{ route('pages.update', $page->id) }}" method="post">
            @csrf
            @method('PATCH')
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="">Page Title</label>
                        <input type="text" name="title" id="" class="form-control" required value="{{$page->title}}" placeholder="Title">
                    </div>
                    <div class="form-group">
                        <label for="">SEO Description (optional)</label>
                        <input type="text" name="seo_description" id="" class="form-control" value="{{$page->seo_description}}" placeholder="SEO Description (optional)">
                    </div>
                    <div class="form-group">
                        <label for="">Page Content</label>
                        <textarea name="content" class="summernote" id="summernote">{{$page->content}}</textarea>
                    </div>
                    <br>
                    <p style="font-style: italic">For Developers...</p>
                    <div class="form-group">
                        <label for="">Style Sheet</label> <br>
                        <textarea name="styles" id="styles" cols="110" rows="30">{{$page->styles}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="">JavaScript</label> <br>
                        <textarea name="javascript" id="javascript" cols="110" rows="30">{{$page->javascript}}</textarea>
                    </div>
                </div>

                {{-- Second column --}}
                <div class="col-md-4">
                    <div class="box box-info">
                        <div class="box-header">
                            <h4>Publish</h4>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <button class="btn btn-default">Save Draft</button>
                                <button class="btn btn-default pull-right">Preview</button>
                            </div>
                            <br>

                            <div class="form-group">
                                <i class="fa fa-info-circle"></i> <label for="">Status: </label>
                                <select name="status" id="" class="form-control">
                                    <option value="draft" {{ $page->status=='draft'?'selected':'' }}>Draft</option>
                                    <option value="published" {{ $page->status=='published'?'selected':'' }} >Published</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <i class="fa fa-eye"></i> <label for="">Visibility: </label>
                                <select name="visibility" id="" class="form-control">
                                    <option value="private" {{ $page->visibility=='private'?'selected':'' }}>Private</option>
                                    <option value="public" {{ $page->visibility=='public'?'selected':'' }} >Public</option>
                                    <option value="published" {{ $page->visibility=='published'?'selected':'' }} >Published</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">Update</button>
                        </div>
                    </div>

                </div>
            </div>      
        </form>
    </section>
@endsection

@section('scripts')
    <script src="{{asset('summernote/dist/summernote.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                height:450,
                placeholder: 'Page content'
            });
        });
    </script>
@endsection