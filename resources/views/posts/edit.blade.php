@extends('layouts.dashboard')

@section('styles')
    <link href="{{asset('summernote/dist/summernote.css')}}" rel="stylesheet">
    <link href="{{asset('css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
@endsection

@section('content')
    @include('partials.errorbag')
    @include('partials.successmsg')

    <section class="content-header">
        <h1>Edit Post</h1>
    </section>

    {{-- Main content --}}
    <section class="content">
        <form action="{{ route('posts.update', $post->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="">Post Title</label>
                        <input type="text" name="title" id="" class="form-control" required value="{{$post->title}}" placeholder="Post Title">
                    </div>
                    <div class="form-group">
                        <label for="">SEO Description (optional)</label>
                        <input type="text" name="seo_description" id="" class="form-control" value="{{$post->seo_description}}" placeholder="SEO Description (optional)">
                    </div>
                    <div class="form-group">
                        <label for="">Post Content</label>
                        <textarea name="content" class="summernote" id="summernote">{{$post->content}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Post Tags</label>
                        <select name="tags[]" class="form-control select2" multiple>
                            @foreach ($tags as $tag)
                                <option value="{{ $tag->id }}" {{in_array($tag->id, $post->tagLists())?'selected':''}}>{{ $tag->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                {{-- Second column --}}
                <div class="col-md-4">
                    <div class="box box-info">
                        <div class="box-header">
                            <h4>Post Attributes</h4>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Featured Image</label>
                                <img src="{{asset('storage/'.$post->featured_image)}}" class="img-rounded" alt="Post image" width="100">
                                <input type="file" name="featured_image">
                            </div>
                            <div class="form-group" id="vue-area">
                                <label for="">Categories</label>
                                <div style="border:1px solid #a9a9a9; width:100%; padding: 2%; height: 200px; overflow-y: scroll">
                                    {{-- <input type="hidden" value="0" name="categoryids[]"> <!--Just in case user does not select any category--> --}}
                                    <template v-for="category in categories">
                                        <input type="checkbox" name="categoryids[]" v-bind:value="category.id" :checked="postcategories.map(a=>a.id).includes(category.id)"> @{{category.name}} <br>
                                    </template>
                                </div>
                                
                                <h5 v-on:click="ok = !ok" style="cursor: pointer; color:#00a65a;"><i class="fa fa-plus"></i> Add new category</h5>
                                
                                <div v-show="ok">
                                    <div class="form-group" :class="{'has-error': hasError}">
                                        <input type="text" v-model="category_name" class="form-control" placeholder="Category Name" ref="newcategory_textbox">
                                    </div>
        
                                    <div class="form-group">
                                        <label for="">Parent Category</label>
                                        <select name="parent_category" id="" class="form-control" v-model="parent_category">
                                            <option value="0" selected>None</option>
                                            <option :value="category.id" v-for="category in categories">@{{category.name}}</option>
                                        </select>
                                    </div>
        
                                    <div class="form-group">
                                        <button type="button" class="btn btn-default" v-on:click="savecategory" :disabled="button_disabled">Add new category</button>
                                    </div>

                                    <small class="label bg-green" v-if="successmsg">Category created successfully</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Comment Status</label>
                                <input type="radio" name="comment_status" value="open" {{ $post->comment_status=='open' ? 'checked' : '' }}> Open &nbsp;
                                <input type="radio" name="comment_status" value="closed" {{ $post->comment_status=='closed' ? 'checked' : '' }}> Closed
                            </div>
                        </div>  
                    </div>


                    <div class="box box-info">
                        <div class="box-header">
                            <h4>Publish</h4>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <button class="btn btn-default">Save Draft</button>
                                <button class="btn btn-default pull-right">Preview</button>
                            </div>
                            <br>

                            <div class="form-group">
                                <i class="fa fa-info-circle"></i> <label for="">Status: </label>
                                <select name="status" id="" class="form-control">
                                    <option value="draft" {{ $post->status=='draft'?'selected':'' }}>Draft</option>
                                    <option value="published" {{ $post->status=='published'?'selected':'' }}>Published</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <i class="fa fa-eye"></i> <label for="">Visibility: </label>
                                <select name="visibility" id="" class="form-control">
                                    <option value="private" {{ $post->visibility=='private'?'selected':'' }}>Private</option>
                                    <option value="public" {{ $post->visibility=='public'?'selected':'' }}>Public</option>
                                    <option value="published" {{ $post->visibility=='published'?'selected':'' }}>Published</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="box-footer">
                            <div class="form-group">
                                <button type="submit" class="btn btn-info pull-right">Update</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>      
        </form>
    </section>
@endsection

@section('scripts')
    <script src="{{asset('summernote/dist/summernote.js')}}"></script>
    <script src="{{asset('js/moment.js')}}"></script>
    <script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                height:450,
                placeholder: 'Post content'
            });
        });
    </script>

    <script>
        $('.select2').select2({
            placeholder: 'Choose a tag',
            tags: true,
        });
    </script>

    <script>
        new Vue({
            el: '#vue-area',
            data: {
                categories: [],
                postcategories: [],
                ok:false,
                category_name: '',
                parent_category: 0,
                button_disabled:false,
                hasError: false,
                successmsg: false,
            },
            methods:{
                savecategory(){
                    this.successmsg = false;
                    if (this.category_name!='') {
                        this.button_disabled=true,
                        this.hasError=false;
                        axios.post("{{url('api/category/create')}}", {
                            newcategory: this.category_name,
                            parent_category: this.parent_category,
                        })
                        .then(response=>{
                            console.log(response.data);
                            this.button_disabled=false;
                            this.category_name='';
                            this.successmsg = true;
                            this.getcategories();
                        });
                    }else{
                        this.$refs.newcategory_textbox.focus();
                        this.hasError=true;
                    }
                },
                getcategories(){
                    axios.get("{{url('api/categories')}}")
                    .then(response=>{
                        console.log(response.data);
                        this.categories = response.data;
                    });
                },
                getpostcategories(){
                    axios.get("{{url('api/postcategories?id='.$post->id)}}")
                    .then(response=>{
                        console.log(response.data);
                        this.postcategories = response.data;
                    });
                },
            },
            mounted: function() {
                this.getcategories();
                this.getpostcategories();
            }
        });
    </script>
@endsection