@extends('layouts.dashboard')

@section('styles')
    <link href="{{asset('summernote/dist/summernote.css')}}" rel="stylesheet">
@endsection

@section('content')
    @include('partials.errorbag')
    @include('partials.successmsg')

    <section class="content-header">
        <h1>Add New Post</h1>
    </section>

    {{-- Main content --}}
    <section class="content">
        <form action="{{ route('posts.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="">Post Title</label>
                        <input type="text" name="title" id="" class="form-control" required placeholder="Post Title">
                    </div>
                    <div class="form-group">
                        <label for="">SEO Description (optional)</label>
                        <input type="text" name="seo_description" id="" class="form-control" placeholder="SEO Description (optional)">
                    </div>
                    <div class="form-group">
                        <label for="">Post Content</label>
                        <textarea name="content" class="summernote" id="summernote"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Post Tags</label>
                        <select name="tags[]" class="form-control select2"  multiple>
                            @foreach ($tags as $tag)
                                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    
                </div>

                {{-- Second column --}}
                <div class="col-md-4">
                    <div class="box box-info">
                        <div class="box-header">
                            <h4>Post Attributes</h4>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Featured Image</label>
                                <input type="file" name="featured_image">
                            </div>
                            <div class="form-group" id="vue-area">
                                <label for="">Categories</label>
                                <div style="border:1px solid #a9a9a9; width:100%; padding: 2%; height: 200px; overflow-y: scroll">
                                    {{-- <input type="hidden" value="0" name="categoryids[]"> <!--Just in case user does not select any category--> --}}
                                    <template v-for="category in categories">
                                        <input type="checkbox" name="categoryids[]" v-bind:value="category.id"> @{{category.name}} <br>
                                    </template>
                                </div>
                                
                                <h5 v-on:click="ok = !ok" style="cursor: pointer; color:#00a65a;"><i class="fa fa-plus"></i> Add new category</h5>
                                
                                <div v-show="ok">
                                    <div class="form-group" :class="{'has-error': hasError}">
                                        <input type="text" v-model="category_name" class="form-control" placeholder="Category Name" ref="newcategory_textbox">
                                    </div>
        
                                    <div class="form-group">
                                        <label for="">Parent Category</label>
                                        <select name="parent_category" id="" class="form-control" v-model="parent_category">
                                            <option value="0" selected>None</option>
                                            <option :value="category.id" v-for="category in categories">@{{category.name}}</option>
                                        </select>
                                    </div>
        
                                    <div class="form-group">
                                        <button type="button" class="btn btn-default" v-on:click="savecategory" :disabled="button_disabled">Add new category</button>
                                    </div>
                                    <small class="label bg-green" v-if="successmsg">Category created successfully</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Comment Status</label>
                                <input type="radio" name="comment_status" value="open" checked> Open &nbsp;
                                <input type="radio" name="comment_status" value="closed"> Closed
                            </div>
                        </div>
                    </div>


                    <div class="box box-info">
                        <div class="box-header">
                            <h4>Publish</h4>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <button class="btn btn-default">Save Draft</button>
                                <button class="btn btn-default pull-right">Preview</button>
                            </div>
                            <br>

                            <div class="form-group">
                                <i class="fa fa-info-circle"></i> <label for="">Status: </label>
                                <select name="status" id="" class="form-control">
                                    <option value="draft">Draft</option>
                                    <option value="published" selected>Published</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <i class="fa fa-eye"></i> <label for="">Visibility: </label>
                                <select name="visibility" id="" class="form-control">
                                    <option value="private">Private</option>
                                    <option value="public">Public</option>
                                    <option value="published" selected>Published</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="box-footer">
                            <div class="form-group">
                                <button type="submit" class="btn btn-info pull-right">Publish</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>      
        </form>
    </section>
@endsection

@section('scripts')
    <script src="{{asset('summernote/dist/summernote.js')}}"></script>
    <script src="{{asset('js/moment.js')}}"></script>

    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                height:450,
                placeholder: 'Post content'
            });
        });
    </script>

    <script>
        $('.select2').select2({
            placeholder: 'Select a tag',
            tags: true,
        });
    </script>

    <script>
        new Vue({
            el: '#vue-area',
            data: {
                categories: [],
                ok:false,
                category_name: '',
                parent_category: 0,
                button_disabled:false,
                hasError: false,
                successmsg: false,
            },
            methods:{
                savecategory(){
                    this.successmsg = false;
                    if (this.category_name!='') {
                        this.button_disabled=true,
                        this.hasError=false;
                        axios.post("{{url('api/category/create')}}", {
                            newcategory: this.category_name,
                            parent_category: this.parent_category,
                        })
                        .then(response=>{
                            console.log(response.data);
                            this.button_disabled=false;
                            this.category_name='';
                            this.successmsg = true;
                            this.getcategories();
                        });
                    }else{
                        this.$refs.newcategory_textbox.focus();
                        this.hasError=true;
                    }
                },
                getcategories(){
                    axios.get("{{url('api/categories')}}")
                    .then(response=>{
                        console.log(response.data);
                        this.categories = response.data;
                    });
                },
            },
            mounted: function() {
                this.getcategories();
            }
        });
    </script>
@endsection