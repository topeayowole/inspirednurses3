@extends('layouts.dashboard')

@section('content')
    <section class="content-header">
        <h1>View Post <a href="{{route('posts.edit', $post->id)}}" class="btn btn-info">Edit</a></h1>
    </section>

    {{-- Main content --}}
    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">{{ $post->title }}</h3>
            </div>
            <div class="box-body" style="text-align: justify;">
                {!! $post->content !!}
            </div>
        </div>
    </section>
@endsection