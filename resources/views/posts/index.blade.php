@extends('layouts.dashboard')

@section('styles')
    @include('partials.datatableStyles')
@endsection

@section('content')
    @include('partials.successmsg')
    <section class="content-header">
        <h1>Posts <a href="{{route('posts.create')}}" class="btn btn-info">Add New</a></h1> 
    </section>

    {{-- Main content --}}
    <section class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table-responsive">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="" id=""></th>
                                <th>Featured Image</th>
                                <th>Title</th>
                                <th><i class="ion ion-ios-eye"></i></th>
                                <th>Author</th>
                                <th>Categories</th>
                                <th>Status</th>
                                <th>Published</th>
                                <th>Updated At</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($posts as $post)
                                <tr>
                                    <td><input type="checkbox" name="" id=""></td>
                                    <td>
                                        {{-- @if(Storage::disk('public')->exists($post->featured_image)) --}}
                                            <img src="{{asset('storage/'.$post->featured_image)}}" class="img-rounded" alt="Post image" width="100">
                                        {{-- @else
                                            <img src="{{asset('storage/post_images/default-post-image.jpg')}}" class="img-rounded" alt="Post image"  width="100px" />
                                        @endif --}}
                                    </td>
                                    <td>
                                        <a href="{{ route('posts.edit', $post->id) }}">{{ $post->title }}</a> 
                                    </td>
                                    <td>{{ $post->postViews()->count() }}</td>
                                    <td>{{ $post->user->firstname ?? $post->user->username }} {{ $post->user->lastname }}</td>
                                    <td> 
                                        @foreach($post->categories as $postcategories) 
                                            - {{ $postcategories->name.' ' }}<br>
                                        @endforeach
                                    </td>
                                    <td>{{ ucfirst($post->status) }}</td>
                                    <td>Published {{ $post->created_at->format('j F, Y h:iA') }}</td> <!-- Different date format - diffForHumans('j F, Y')-->
                                    <td>Updated at {{ $post->updated_at->format('j F, Y h:iA') }}</td> <!-- Different date format - diffForHumans('j F, Y')-->
                                    <td>
                                        <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                    </td>
                                    <td>
                                        <form id="deleteForm" method="post" action="{{ route('posts.destroy', $post->id) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger" onClick='return confirm("Are you sure you want to delete?")'><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    @include('partials.datatableScripts')
@endsection
