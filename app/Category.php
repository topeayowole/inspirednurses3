<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
    use Sluggable;

    protected $fillable = ['name', 'parent_id', 'image', 'slug'];

    public function parentcategory(){
        return $this->belongsTo('App\Category', 'parent_id');
    }

    public function posts(){
        return $this->belongsToMany('App\Post');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
