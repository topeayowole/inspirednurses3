<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    // public function __construct(){
    //     $this->middleware('auth', ['except'=>['show']]);
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::latest()->get();
        return view('categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('name')->get();
        return view('categories.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate
        $this->validate(request(), [
            'name'=>'required|min:3',
        ]);

        Category::create([
            'name'=>$request->name,
            'parent_id'=>$request->parent_id,
            'image'=>$request->file('image')==null? null : $request->file('image')->storeAs('category_images//', uniqid().'.'.$request->file('image')->getClientOriginalExtension(), 'public'),
        ]);

        return redirect()->route('categories.index')->with('success', 'Category created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categories = Category::orderBy('name')->get();
        return view('categories.edit', compact('category', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //Validate
        $this->validate(request(), [
            'name'=>'required|min:3',
        ]);

        if ($request->file('image')!=null) {
            //Delete the former image.
            Storage::disk('public')->delete($category->image);
        }

        $category->update([
            'name'=>$request->name,
            'parent_id'=>$request->parent_id,
            'image'=>$request->file('image')==null? $category->image : $request->file('image')->storeAs('category_images//', uniqid().'.'.$request->file('image')->getClientOriginalExtension(), 'public'),
            'slug' => null,
        ]);

        return redirect()->route('categories.index')->with('success', 'Category updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //Delete the image 
        Storage::disk('public')->delete($category->image);

        $category->delete();

        return redirect()->route('categories.index')->with('success', 'Category deleted successfully');
    }
}
