<?php

namespace App\Http\Controllers;

use App\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::orderBy('order')->get();
        return view('menus.index', compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menus = Menu::all();
        return view('menus.create', compact('menus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate
        $this->validate(request(), [
            'name'=>'required|min:3',
        ]);

        //The id you are trying to target cannot be the parent at the same time.
        // if ($request->menu_target_id == $request->parent_id) {
        //     return back()->withErrors(['parent_id'=>'Parent menu cannot be the same as the target menu']);
        // }

        Menu::create([
            'name'=>$request->name,
            'parent_id'=>$request->parent_id,
            'order'=>$request->order,
            'menu_type'=>$request->menu_type,
            'menu_target_id'=>$request->menu_target_id,
            'external_url'=>$request->external_url,
            'is_homepage'=>$request->is_homepage,
        ]);

        return redirect()->route('menus.index')->with('success', 'Menu created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        $menus = Menu::all();
        return view('menus.edit', compact('menu', 'menus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        //Validate
        $this->validate(request(), [
            'name'=>'required|min:3',
        ]);

        $menu->update([
            'name'=>$request->name,
            'parent_id'=>$request->parent_id,
            'order'=>$request->order,
            'menu_type'=>$request->menu_type,
            'menu_target_id'=>$request->menu_target_id,
            'external_url'=>$request->external_url,
            'is_homepage'=>$request->is_homepage,
            'slug' => null,
        ]);

        return redirect()->route('menus.index')->with('success', 'Menu updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $menu->delete();

        return redirect()->route('menus.index')->with('success', 'Menu deleted successfully');
    }
}
