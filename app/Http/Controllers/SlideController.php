<?php

namespace App\Http\Controllers;

use App\Slide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = Slide::orderBy('order')->get();
        return view('slides.index', compact('slides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('slides.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
         $this->validate(request(), [
            'image'=>'required',
        ]);

        Slide::create([
            'caption'=>$request->caption,
            'subcaption'=>$request->subcaption,
            'image'=>$request->file('image')->storeAs('slides//', uniqid().'.'.$request->file('image')->getClientOriginalExtension(), 'public'),
            'visibility'=>$request->visibility,
            'order'=>$request->order,
        ]);

        return redirect()->route('slides.index')->with('success', 'Slide created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function show(Slide $slide)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function edit(Slide $slide)
    {
        return view('slides.edit', compact('slide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slide $slide)
    {
        if ($request->file('image')!=null) {
            //Delete the former image.
            Storage::disk('public')->delete($slide->image);
        }

        $slide->update([
            'caption'=>$request->caption,
            'subcaption'=>$request->subcaption,
            'image'=>$request->file('image')==null? $slide->image :$request->file('image')->storeAs('slides//', uniqid().'.'.$request->file('image')->getClientOriginalExtension(), 'public'),
            'visibility'=>$request->visibility,
            'order'=>$request->order,
        ]);

        return redirect()->route('slides.index')->with('success', 'Slide updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slide $slide)
    {
        //Delete the image 
        Storage::disk('public')->delete($slide->image);

        $slide->delete();

        return redirect()->route('slides.index')->with('success', 'Slide deleted successfully');
    }
}
