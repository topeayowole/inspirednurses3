<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Page;
use Auth;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pages = Page::latest()->get();

        return view('pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate
        $this->validate(request(), [
            'title'=>'required|min:3',
            'content' => 'required',
        ]);


        Page::create([
            'user_id' => Auth::user()->id,
            'title' => $request->title,
            'seo_description' => $request->seo_description,
            'content' => $request->content,
            'styles' => $request->styles,
            'javascript' => $request->javascript,
            'status' => $request->status,
            'visibility' => $request->visibility,
            'updated_by' => Auth::user()->id,
        ]);

        return redirect()->route('pages.index')->with('success', 'Page created successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        //
        //$page = Page::find($id);
        return view('pages.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        //Validate
        $this->validate(request(), [
            'title'=>'required|min:3',
            'content' => 'required',
        ]);

        $page->update([
            'title' => $request->title,
            'seo_description' => $request->seo_description,
            'content' => $request->content,
            'styles' => $request->styles,
            'javascript' => $request->javascript,
            'status' => $request->status,
            'visibility' => $request->visibility,
            'updated_by' => Auth::user()->id,
            'slug' => null, //This makes the slug automatically update with the title. If you don't want the slug to be auto-updated, comment it out.
        ]);

        return back()->with('success', 'Page updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        //$page = Page::find($id);
        
        $page->delete();

        return redirect()->route('pages.index')->with('success', 'Page deleted successfully');
    }
}
