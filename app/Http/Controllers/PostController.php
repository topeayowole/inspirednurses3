<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\Tag;
use App\Setting;
use Illuminate\Support\Facades\Storage;
use Auth;

class PostController extends Controller
{
    // public function __construct(){
    //     $this->middleware('auth', ['except'=>['show']]);
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('updated_at', 'desc')->get();

        //dd($posts);

        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::orderBy('name')->get();
        return view('posts.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->file('featured_image'));
        //validate
        $this->validate(request(), [
            'title'=>'required|min:3',
            'content' => 'required',
            'featured_image'=>'mimes:jpg,png,jpeg',
        ]);

        $post = Post::create([
            'user_id' => Auth::user()->id,
            'title' => $request->title,
            'seo_description' => $request->seo_description,
            'content' => $request->content,
            //'featured_image' => $request->file('featured_image')==null? null :$request->file('featured_image')->storeAs('post_images//', uniqid().'.jpg', 'public'),
            'comment_status' => $request->comment_status,
            'status' => $request->status,
            'visibility' => $request->visibility,
            'updated_by' => Auth::user()->id,
        ]);

        //I set the 'featured_image' column in the database to a default of "post_images/default-post-image.jpg"
        if ($request->hasFile('featured_image')) {
            $post->update([
                'featured_image' => $request->file('featured_image')->storeAs('post_images//', uniqid().'.jpg', 'public'),
            ]);    
        }

        //If there are tags coming from the frontend, otherwise, it will throw an...
        //error trying to call foreach on an empty array.
        if ($request->tags) {
            //dd($request->tags) is in this order:
            //array:3 [▼
            //  0 => "3"     ---> exist
            //  1 => "2"     ---> exist
            //  2 => "Mental Wellbeing"   ---> does not exist
            // ]
            //For each tag, create it in the "tags" table.
            foreach ($request->tags as $tagId) {
                //Check if the tag exists.
                //NB: If the tag doesn't exist, the tagId will be the name of the tag.
                if (Tag::find($tagId)==null) {
                    // If it does not exist, create a new tag with the tag name.
                    //In this case, the tagId will be the tag name.
                    $tag = Tag::create([
                        'name' => $tagId,
                    ]);
                    
                    // Attach it
                    $post->tags()->attach($tag->id);
                }else{

                    //Just attach the tag id you got.
                    $post->tags()->attach($tagId);
                }

            }
        }

        $post->categories()->attach($request->categoryids);

        return redirect()->route('posts.index')->with('success', 'Post created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
        //$themename = Setting::find(1)->themename;
        //I had to use double quotes because of the variable name.
        //return view("frontend.themes.$themename.single-post", compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $tags = Tag::orderBy('name')->get();
        return view('posts.edit', compact('post', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        // dd($request->file('featured_image'));
        // dd($post->featured_image);
        //validate
        $this->validate(request(), [
            'title'=>'required|min:3',
            'content' => 'required',
            'featured_image'=>'mimes:jpg,png,jpeg',
        ]);

        if ($request->file('featured_image')!=null) {
            //Delete the former image.
            Storage::disk('public')->delete($post->featured_image);
        }

        $post->update([
            'title' => $request->title,
            'seo_description' => $request->seo_description,
            'content' => $request->content,
            'featured_image' => $request->file('featured_image')==null? $post->featured_image : $request->file('featured_image')->storeAs('post_images/', uniqid().'.jpg', 'public'),
            'comment_status' => $request->comment_status,
            'status' => $request->status,
            'visibility' => $request->visibility,
            'updated_by' => Auth::user()->id,
            'slug' => null, //This makes the slug to automatically update with the title. If you don't want the slug to be auto-updated, comment it out.
        ]);


        //If there are tags coming from the frontend, otherwise, it will throw an...
        //error trying to call foreach on an empty array.
        if ($request->tags) {
            //dd($request->tags); //is in this order:
            //array:3 [▼
            //  0 => "3"     ---> exist
            //  1 => "2"     ---> exist
            //  2 => "Mental Wellbeing"   ---> does not exist
            // ]

            //Now, first of all, create an array to store tag Ids.
            $tagIds = [];

            //For each tag
            foreach ($request->tags as $tagId ) {
                //Find out if the tag exists.
                //NB: If the tag doesn't exist, the tagId will be the name of the tag.
                if (Tag::find($tagId)==null) {
                    //Create the tag
                    $tag = Tag::create([
                        'name' => $tagId,
                    ]);
                    
                    //Add the newly generated id to an array of tag ids.
                    $tagIds[] =  $tag->id;
            
                }else{
                    //Else, if the tag exists, just add the existing Id to the array of tag Ids.
                    
                    $tagIds[] = $tagId;
                }
            }
            
            //At the end of the day, sync all of them at once.
            $post->tags()->sync($tagIds);

            //If I had done it with the way I approached it in the store() method above,
            //the sync method will be executed for each tag Id in the loop.
            //At the end of the day, it is only the last tag id that stays in the database
            //Why? because the sync() method has the ability to delete.
        }else{
            //If there are no tags from the frontend, delete or detach all existing tags.
            
            
            //First of all check if there are tags related to this post otherwise, it will ...
            //throw an error when trying to call detach on a post without tags.
            if ($post->tags) {
                //Then detach all tags
                $post->tags()->detach();
            }
        }
        
        $post->categories()->sync($request->categoryids);

        return redirect()->route('posts.index')->with('success', 'Post updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //Delete the image 
        Storage::disk('public')->delete($post->image);

        $post->categories()->detach();

        $post->delete();

        return redirect()->route('posts.index')->with('success', 'Post deleted successfully');
    }
}
