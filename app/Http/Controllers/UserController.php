<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Storage;
use Image;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate
        $this->validate(request(), [
            'firstname'=>'required|min:3',
            'lastname'=>'required|min:3',
            'email'=>'required|email|unique:users',
            'username'=>'required|min:3',
            'password'=>'required|min:3|confirmed',
            'phone'=>'required',
        ]);

        $user = User::create([
            'firstname'=>$request->firstname,
            'lastname'=>$request->lastname,
            'email'=>$request->email,
            'username'=>$request->username,
            'password'=>bcrypt($request->password),
            'role'=>$request->role,
            'phone'=>$request->phone,
        ]);
        
        //If the user photo was uploaded
        if ($request->file('photo')!=null) {
            $user->update([
                'photo'=>$request->file('photo')->storeAs('users/', $request->username.'.jpg', 'public')
            ]);

            //Resize the photo
            if ($request->file('photo')!=null && $user->photo!='users/avatar.png') {
                $image = Image::make($request->file('photo'));
                $image->resize(250, 250);
                $image->save(public_path().'/storage/users/'.$user->username.'.jpg');
            }
        }

        return redirect()->route('users.index')->with('success', 'User successfully created');
        
    }


    public function profile(User $user){
        return view('users.profile', compact('user'));
    }

    public function updateProfile(Request $request, User $user){
        //Validate
        $this->validate(request(), [
            'firstname'=>'required|min:3',
            'lastname'=>'required|min:3',
            'email'=>'required|email',
            'username'=>'required|min:3',
            'phone'=>'required',
            'photo'=>'mimes:jpg,png,jpeg'
        ]);

        if ($request->file('photo')!=null && $user->photo!='users/avatar.png') {
            //Delete the former image.
            Storage::disk('public')->delete($user->photo);
        }

        $user->update([
            'firstname'=>$request->firstname,
            'lastname'=>$request->lastname,
            'email'=>$request->email,
            'username'=>$request->username,
            'role'=>$request->role,
            'phone'=>$request->phone,
            'photo'=>$request->file('photo')==null ? $user->photo : $request->file('photo')->storeAs('users/', $request->username.'.jpg', 'public'),
        ]);
        
        //Resize the photo
        if ($request->file('photo')!=null && $user->photo!='users/avatar.png') {
            $image = Image::make($request->file('photo'));
            $image->resize(250, 250);
            $image->save(public_path().'/storage/users/'.$user->username.'.jpg');
        }

        return back()->with('success', 'User profile updated successfully.');
    }

    public function changePassword(User $user){
        return view('users.change-password', compact('user'));
    }

    public function postChangePassword(Request $request, User $user){
        //Validate
        $this->validate(request(), [
            'oldpassword'=>'required',
            'password'=>'required|min:3|confirmed',
        ]);

        //Verify the old password against the hash
        if (password_verify($request->oldpassword, $user->password)) {
            //Update the user password with the new password
            $user->update([
                'password'=>bcrypt($request->password)
            ]);

            return back()->with('success', 'Password Changed Successfully');
        }else{
            return back()->withErrors(['error'=>'Invalid Old Password']);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //Validate
        $this->validate(request(), [
            'photo'=>'mimes:jpg,png,jpeg'
        ]);

        if ($request->file('photo')!=null && $user->photo!='users/avatar.png') {
            //Delete the former image.
            Storage::disk('public')->delete($user->photo);
        }

        $user->update([
            'firstname'=>$request->firstname,
            'lastname'=>$request->lastname,
            'email'=>$request->email,
            'username'=>$request->username,
            'role'=>$request->role,
            'phone'=>$request->phone,
            'photo'=>$request->file('photo')==null ? $user->photo : $request->file('photo')->storeAs('users/', $request->username.'.jpg', 'public'),
        ]);

        //Resize the photo
        if ($request->file('photo')!=null && $user->photo!='users/avatar.png') {
            $image = Image::make($request->file('photo'));
            $image->resize(250, 250);
            $image->save(public_path().'/storage/users/'.$user->username.'.jpg');
        }

        return back()->with('success', 'User successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //Delete the user's image
        if ($user->photo!='users/avatar.png') {
            Storage::disk('public')->delete($user->photo);
        }

        $user->delete();

        return redirect()->route('users.index')->with('success', 'User deleted successfully');
    }
}
