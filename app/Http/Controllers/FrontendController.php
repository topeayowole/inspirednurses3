<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\Slide;
use App\Page;
use App\Post;
use App\Category;
use App\Setting;
use App\Tag;
use App\FrontendPage;
use App\PostView;

class FrontendController extends Controller
{

    public function __construct(){
        // $pages = Page::orderBy('position')->where('status', 'published')->get();

        // $setting = Setting::find(1);
        // $themename = $setting->themename;
        // $sitename = $setting->sitename;
        // $menus = Menu::orderBy('order')->get();
        // $recentposts = Post::latest()->take(5)->get();
        // $tags = Tag::orderBy('name')->get();
        // // dd($samplepostlist);
        // // //This is to pass variables to the layout too
        // View()->share(compact('menus', 'themename', 'sitename', 'recentposts', 'tags'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Look for the menu that is the homepage
        $menu = Menu::where('is_homepage', 1)->first(); 

        //If there is no menu with homepage set, use the first menu as the home page.
        if ($menu==null) {
            $menu = Menu::all()->first();
        }


        //if menu is not null  
        if ($menu!=null) {
            return $this->urlmapper($menu->slug);
        }

    }



    public function urlmapper($menuslug){

        $menu=Menu::where('slug', $menuslug)->first();
        
        //Incase the slug does not exist
        if ($menu == null) {
            //return view('frontend/themes/'.$theme_name.'/error-404');
            abort('404');
        }

        //Create a new instance of the HomePage class.
        $frontendpage = new FrontendPage;
        $frontendpage->name = $menu->name ?? null;
        $frontendpage->menus = Menu::whereNull('parent_id')->orderBy('order')->get();

        if ($menu->menu_type=='page') {
            $frontendpage->page = Page::findOrFail($menu->menu_target_id); 
        }elseif ($menu->menu_type=='post') {
            $frontendpage->post = Post::findOrFail($menu->menu_target_id);
        }elseif ($menu->menu_type=='post category') {
            $category = Category::findOrFail($menu->menu_target_id);
            $frontendpage->posts = $category->posts()->paginate(10);
        }elseif ($menu->menu_type=='all posts') {
            $frontendpage->posts = Post::where('status', 'published')->paginate(10);
        }
        
        $frontendpage->recentposts = Post::latest()->where('status', 'published')->take(5)->get();

        $frontendpage->setting = Setting::find(1);

        $frontendpage->tags = Tag::orderBy('name')->get();
        
        if ($menu->is_homepage) {
            $frontendpage->slides = Slide::where('visibility', 1)->orderBy('order')->get();
        } 

        if ($menu->menu_type=='external url') {
            $url = $menu->external_url;
            // return Redirect::to($url);
            return redirect()->away($url);
        }
  
        
        return view('frontend.index', compact('frontendpage'));
    }


    public function showpost($slug){
        $post = Post::where('slug', $slug)->first();

        //If the post couldn't be found...
        if ($post==null) {
            abort('404');
        }

        //Create a new instance of the HomePage class.
        $frontendpage = new FrontendPage;
        $frontendpage->name = $post->title;
        $frontendpage->menus = Menu::whereNull('parent_id')->orderBy('order')->get();
        $frontendpage->post = $post;
        $frontendpage->recentposts = Post::latest()->take(5)->get();
        $frontendpage->setting = Setting::find(1);
        $frontendpage->tags = Tag::orderBy('name')->get();

        //Log the visitor who visited the post
        PostView::createViewLog($post);

        return view('frontend.index', compact('frontendpage'));
    }

    public function showcategoryposts($slug){
        $category = Category::where('slug', $slug)->first();

        //If the category couldn't be found...
        if ($category==null) {
            abort('404');
        }

        //Create a new instance of the HomePage class.
        $frontendpage = new FrontendPage;
        $frontendpage->name = $category->title;
        $frontendpage->menus = Menu::whereNull('parent_id')->orderBy('order')->get();
        $frontendpage->posts = $category->posts()->paginate(10);
        $frontendpage->recentposts = Post::latest()->take(5)->get();
        $frontendpage->setting = Setting::find(1);
        $frontendpage->tags = Tag::orderBy('name')->get();


        return view('frontend.index', compact('frontendpage'));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
