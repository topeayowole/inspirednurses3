<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use App\Events\MessageSent;

class ChatController extends Controller
{
    //
    public function send(Request $request){
        
        //message is being sent
        $message = Message::create([
            'to' => $request->to,
            'from' => $request->from,
            'message' => $request->message,
        ]);

        event(new MessageSent($message));
    }
    
}
