<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        // $allfiles = scandir('../resources/views/frontend/themes');
        $foldernameswithpath = glob('../resources/views/frontend/themes/*', GLOB_ONLYDIR);
        $foldernames = array_map('basename', $foldernameswithpath);
        //dd($foldernames);
        return view('settings.edit', compact('setting', 'foldernames'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        //validate
        $this->validate(request(), [
            'sitename'=>'required|min:3',
        ]);

        $setting->update([
            'sitename'=> $request->sitename,
            'themename'=> $request->themename,
            'logo'=> $request->file('logo')==null? $setting->logo : $request->file('logo')->storeAs('settings//', 'logo.jpg', 'public'),
            'description'=> $request->description,
            'styles'=> $request->styles,
            'javascript'=> $request->javascript,
        ]);

        return redirect()->back()->with('success', 'Settings Saved successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
