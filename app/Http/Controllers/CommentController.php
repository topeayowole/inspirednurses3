<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::latest()->get();

        return view('comments.index', compact('comments'));
    }

    public function approve(Comment $comment){
        $comment->update([
            'status'=>'Approved'
        ]);

        return redirect()->route('comments.index')->with('success', 'Comment Successfully Approved.');
    }

    public function disapprove(Comment $comment){
        $comment->update([
            'status'=>'Disapproved'
        ]);

        return redirect()->route('comments.index')->with('success', 'Comment Disapproved Successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'content'=>'required'
        ]);

        Comment::create([
            'post_id'=>$request->post_id,
            'user_id'=>Auth::user()->id,
            // 'parent_id'=>'',
            'content'=>$request->content,
        ]);

        return back();
    }

    public function reply(Request $request){

        Comment::create([
            'post_id'=>$request->post_id,
            'user_id'=>Auth::user()->id,
            'parent_id'=>$request->comment_id,
            'content'=>$request->content,
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        return view('comments.show', compact('comment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();

        return redirect()->route('comments.index')->with('success', 'Comment Deleted Successfully');
    }
}
