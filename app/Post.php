<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Post extends Model
{
    //
    use Sluggable;

    protected $fillable = ['user_id', 'title', 'seo_description', 'content', 'featured_image', 'category_id', 'comment_status', 'status', 'visibility', 'slug', 'updated_by'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function categories(){
        return $this->belongsToMany('App\Category');
    }

    public function tags(){
        return $this->belongsToMany('App\Tag');
    }

    public function comments(){
        return $this->hasMany('App\Comment')->whereNull('parent_id');
    }

    public function tagLists(){
        return $this->tags->pluck('id')->all();
    }

    public function postViews(){
        return $this->hasMany('App\PostView');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    //I also want to link to posts using a unique 'slug' rather than the default ID in the URL.
    // public function getRouteKeyName()
    // {
    //     return 'slug';
    // }
}
