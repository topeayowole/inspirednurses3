<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Post;
use App\Page;
use App\Setting;

class Page extends Model
{
    //
    use Sluggable; 

    protected $fillable = [
        'user_id', 'title', 'seo_description', 'content', 'status', 'visibility', 'slug',
        'updated_by', 'styles', 'javascript'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ]
        ];
    }

    //I also want to link to posts using a unique 'slug' rather than the default ID in the URL.
    // public function getRouteKeyName()
    // {
    //     return 'slug';
    // }
}
