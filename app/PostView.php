<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Stevebauman\Location\Facades\Location;
// use Illuminate\Http\Request;

class PostView extends Model
{
    //
    protected $fillable = ['post_id', 'session_id', 'user_id', 'ip', 'country', 'city', 'agent'];

    public static function createViewLog($post){
        $postView = new PostView;
        $postView->post_id = $post->id;
        $postView->session_id = \Request::getSession()->getId();
        $postView->user_id = (Auth::check()) ? Auth::id() : null; //No need to use Auth()->user()->id as we have an inbuilt function to get auth id.
        $postView->ip = \Request::getClientIp();
        $postView->country = Location::get($postView->ip)==null ? NULL : Location::get($postView->ip)->countryName;
        $postView->city = Location::get($postView->ip)==null ? NULL : Location::get($postView->ip)->cityName;
        $postView->agent = \Request::header('User-Agent');
        $postView->save();
    }

    public function post(){
        return $this->belongsTo('App\Post');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

}
