<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Menu extends Model
{
    use Sluggable; 

    protected $fillable = ['name', 'parent_id', 'order', 'menu_type', 'menu_target_id', 'is_homepage', 'external_url',  'slug'];

    public function parent(){
        return $this->belongsTo('App\Menu', 'parent_id');
    }

    public function children(){
        return $this->hasMany('App\Menu', 'parent_id');
    }
    //Eloquent will naturally be looking for "menu_id" in the menus table to get this to work.
    //However, ...
    //Specifying a specific foreign key since there is no "menu_id" in the menus table.

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
