<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $fillable = ['post_id', 'user_id', 'parent_id', 'content', 'status'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function parent(){
        return $this->belongsTo('App\Comment', 'parent_id');
    }
    
    public function replies(){
        return $this->hasMany('App\Comment', 'parent_id');
        //Eloquent will naturally be looking for "comment_id" in the comments table to get this to work.
        //However, ...
        //Specifying a specific foreign key since there is no "comment_id" in the comments table.
    }

    public function post(){
        return $this->belongsTo('App\Post');
    }
}
