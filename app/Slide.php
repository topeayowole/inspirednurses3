<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $fillable = ['caption', 'subcaption', 'image', 'visibility', 'order'];
}
