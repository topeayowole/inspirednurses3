<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'username'=>'Josiah Okesola',
            'email' =>  'josiah@inspirednurses.org',
            'password' => bcrypt('admin123'),
            'role' => 'Admin',
        ]);

        // User::create([
        //     'username'=>'John Doe',
        //     'email' => 'johndoe@gmail.com',
        //     'password' => bcrypt('wav@1234.com.ng'),
        //     'role' => 'Admin',
        // ]);
    }
}
