<?php

use Illuminate\Database\Seeder;
use App\Menu;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Menu::create([
            'name' => 'Home',
            'parent_id' => null,
            'order' => 1,
            'menu_type' => 'page',
            'menu_target_id' => 1,
            'is_homepage' => 1,
            'external_url' => null,
        ]);

        // Menu::create([
        //     'name' => 'Sample Post',
        //     'parent_id' => 1,
        //     'order' => 1,
        //     'menu_type' => 'post',
        //     'menu_target_id' => 1,
        //     'is_homepage' => 0,
        //     'external_url' => null,

        // ]);

        Menu::create([
            'name' => 'All Posts',
            'parent_id' => null,
            'order' => 2,
            'menu_type' => 'all posts',
            'menu_target_id' => 1,
            'is_homepage' => 0,
        ]);

        Menu::create([
            'name' => 'Post Category',
            'parent_id' => null,
            'order' => 3,
            'menu_type' => 'post category',
            'menu_target_id' => 1,
            'is_homepage' => 0,
        ]);

        // Menu::create([
        //     'name' => 'External URL',
        //     'parent_id' => 1,
        //     'order' => 1,
        //     'menu_type' => 'external url',
        //     'menu_target_id' => 1,
        //     'is_homepage' => 0,
        //     'external_url' => 'http://www.google.com',
        // ]);
    }
}
