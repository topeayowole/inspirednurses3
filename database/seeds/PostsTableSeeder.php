<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0; $i < 20; $i++) { 
            $post = Post::create([
                'user_id'=> 1,
                'title' => $faker->sentence($nbWords = 6),
                'seo_description' => $faker->sentence($nbWords = 3),
                'content' => $faker->paragraph($nbSentences=100),
                // 'featured_image' => $faker->image('public/storage/post_images', 'transport'),
                'comment_status' => 'open',
                'status' => 'published',
                'visibility' => 'visible', 
                //'slug' => $faker->unique()->sentence(),
                'updated_by' => 1,
            ]);

            $post->categories()->attach($faker->numberBetween($min = 1, $max = 3));


        }
        
    }
}
