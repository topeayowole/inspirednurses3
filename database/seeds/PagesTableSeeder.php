<?php

use Illuminate\Database\Seeder;
use App\Page;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        for ($i=1; $i <= 10; $i++) {
            Page::create([
                'user_id'=> 1,
                'title'=> 'Page '.$i,
                'seo_description'=> $faker->sentence($nbWords = 6),
                'content'=> $faker->paragraph($nbSentences = 10),
                'status'=> 'published',
                'visibility'=> 'public',
                'updated_by'=> 1,
            ]);
        }
    }
}
