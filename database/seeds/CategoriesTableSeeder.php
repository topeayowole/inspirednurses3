<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();

        Category::create([
            'name' => 'Food',
            'image' => $faker->image($dirs='public/storage/category_images', $width=600, $height=600, null, false),
        ]);

        Category::create([
            'name' => 'Tech',
            'image' => $faker->image($dirs='public/storage/category_images', $width=600, $height=600, null, false),
        ]);

        Category::create([
            'name' => 'Nature',
            'image' => $faker->image($dirs='public/storage/category_images', $width=600, $height=600, null, false),
        ]);
        
    }
}
