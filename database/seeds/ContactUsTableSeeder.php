<?php

use Illuminate\Database\Seeder;
use App\ContactUs;

class ContactUsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();

        for ($i=0; $i < 10; $i++) { 
            ContactUs::create([
                'name' => $faker->name,
                'phone' => $faker->phoneNumber,
                'email' =>  $faker->email,
                'subject' =>  $faker->sentence($nbWords = 6),
                'message' =>  $faker->paragraph($nbSentences=100),
            ]);
        }
    }
}
