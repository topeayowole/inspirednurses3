<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('title');
            $table->string('seo_description')->nullable();
            $table->longText('content');
            $table->longText('styles')->nullable();
            $table->longText('javascript')->nullable();
            $table->string('status'); //published/draft
            $table->string('visibility'); //hidden/semi-hidden/visible
            $table->string('slug')->nullable();
            $table->unsignedBigInteger('updated_by');
            $table->timestamps();

             $table->foreign('user_id')->references('id')
            ->on('users');

             $table->foreign('updated_by')->references('id')
            ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
