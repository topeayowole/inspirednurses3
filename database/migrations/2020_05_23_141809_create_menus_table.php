<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name'); 
            $table->integer('parent_id')->nullable();//0 - means no parent
            $table->integer('order');
            $table->string('menu_type'); //page/single post/post category/all posts/external url
            $table->integer('menu_target_id')->nullable(); 
            $table->boolean('is_homepage')->default(0); 
            $table->string('external_url')->nullable();//external link
            $table->string('slug')->nullable();//external link
            $table->timestamps();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
