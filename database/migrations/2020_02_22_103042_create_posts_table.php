 <?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('title');
            $table->string('seo_description')->nullable();
            $table->longText('content');
            $table->string('featured_image')->default('post_images/default-post-image.jpg');
            $table->string('status'); //draft/published
            $table->string('visibility'); //hidden/semi-hidden/visible
            $table->string('comment_status'); //open/closed
            $table->unsignedBigInteger('updated_by');
            $table->string('slug');
            $table->integer('view_count')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')
            ->on('users');

            $table->foreign('updated_by')->references('id')
            ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
